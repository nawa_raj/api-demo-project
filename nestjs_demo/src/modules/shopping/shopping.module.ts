import { Module } from '@nestjs/common';
import { OrderModule } from './order/order.module';
import { OrderItemModule } from './order-item/order-item.module';




@Module({
  imports: [
    OrderModule,
    OrderItemModule
  ]
})
export class ShoppingModule { }
