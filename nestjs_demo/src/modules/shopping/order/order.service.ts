/* eslint-disable no-unused-vars */
import { Inject, Injectable } from '@nestjs/common';
import { REPOSITORY_KEYS } from 'src/constants/keys';
import Order from 'src/db/models/shopping/Order';
import OrderItem from 'src/db/models/shopping/OrderItem';
import { OrderCreateDto, OrderUpdateDto } from './order.dto';
import { PaginationPropsType } from 'src/interfaces/base.interface';
import { ResponseDataWithPaginationType } from 'src/interfaces/response.interface';





@Injectable()
export class OrderService {
    constructor(
        @Inject(REPOSITORY_KEYS.ORDER_REPOSITORY) private readonly orderRepository: typeof Order,
        @Inject(REPOSITORY_KEYS.ORDER_ITEM_REPOSITORY) private readonly orderItemRepository: typeof OrderItem
    ) { }


    async findAll(user_id?: string): Promise<Order[]> {
        if (user_id) {
            const orders = await this.orderRepository.findAll({ where: { user_id }, include: OrderItem });
            return orders

        } else {
            const orders = await this.orderRepository.findAll({ include: OrderItem });
            return orders
        }
    }

    async findAllWithPagination({ id: user_id, page, perPage }: PaginationPropsType & { id?: string }): Promise<ResponseDataWithPaginationType<Order[]>> {
        const currentPage = page || 1;
        const limit = perPage || 10;
        const offset = (currentPage - 1) * limit;

        const data: { rows: Order[], count: number } = { rows: [], count: 0 }

        if (user_id) {
            const { count, rows } = await this.orderRepository.findAndCountAll({
                where: { user_id },
                include: OrderItem,
                offset,
                limit
            });
            data.count = count; data.rows = rows;

        } else {
            const { count: count1, rows: rows1 } = await this.orderRepository.findAndCountAll({
                include: OrderItem,
                offset,
                limit
            });
            data.count = count1; data.rows = rows1;
        }

        const totalPages = Math.ceil(data.count / limit);

        return {
            total_pages: totalPages,
            page: currentPage,
            per_page: limit,
            data: data.rows,
        }
    }


    async findOneById(id: string): Promise<Order> {
        const order = await this.orderRepository.findByPk(id, { include: OrderItem });
        return order
    }


    async create(order: OrderCreateDto): Promise<any> {
        const orderItemsInReq = order.order_items;
        let totalQty = 0;
        let totalPrice = 0;

        // FIND LAST ORDER NUMBER AND INCREMENT OF THAT AND ASSIGN TO NEW ORDER...
        const lastOrder = await this.orderRepository.findOne({ order: [['created_at', 'DESC']] });
        const order_number = (lastOrder ? lastOrder.order_number + 1 : 1);

        orderItemsInReq.forEach((item) => {
            totalQty += item.quantity;
            totalPrice += item.total_price;
        })

        const newOrder = await this.orderRepository.create<Order>({
            user_id: order.user_id,
            status: order.status,
            total_price: totalPrice,
            total_quantity: totalQty,
            order_number,
        });

        const refinedOrderItems = orderItemsInReq.map((item) => {
            return { ...item, order_id: newOrder.id }
        });

        // LET CREATE ORDERITEMS FOR ORDER WITH NEWLY CREATED ORDER ID...
        const newOrderItems = await this.orderItemRepository.bulkCreate(refinedOrderItems);

        // SET ALL CREATED ORDERITEMS TO THE ORDER...
        await newOrder.$set('order_items', newOrderItems);

        const user = await newOrder.$get("user", { attributes: { exclude: ['password'] } });

        return {
            ...newOrder.toJSON(),
            order_items: newOrderItems,
            user
        };
    }


    async update(id: string, data: OrderUpdateDto) {
        const [numberOfAffectedRows, [updatedOrder]] = await this.orderRepository.update({ ...data }, { where: { id }, returning: true });
        // const user = await updatedOrder.$get("user", { attributes: { exclude: ["password"] } });
        const orderItems = await updatedOrder.$get("order_items");

        return {
            numberOfAffectedRows,
            updatedOrder: {
                ...updatedOrder.toJSON(),
                // user,
                order_items: orderItems
            }
        };
    }


    async delete(id: string) {
        return await this.orderRepository.destroy({ where: { id } });
    }
}
