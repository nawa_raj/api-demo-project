/* eslint-disable no-unused-vars */
import {
    Body, Controller, Delete, Get, NotFoundException, Param,
    ParseUUIDPipe, Patch, Post, Query, Req, Request, Res,
    ValidationPipe
} from '@nestjs/common';
import { IsOptional, IsUUID } from 'class-validator';
import { Response as ExpResponse } from 'express';
import Order from 'src/db/models/shopping/Order';
import { OrderCreateDto, OrderUpdateDto } from './order.dto';
import { OrderService } from './order.service';
import { PaginationQueryParams } from 'src/services/validation/request';


class FindAllQueryParams extends PaginationQueryParams {
    @IsOptional()
    @IsUUID()
    id?: string;
}


@Controller('orders')
export class OrderController {
    constructor(private readonly orderService: OrderService) { }

    @Get()
    async findAll(@Query(new ValidationPipe({ transform: true })) params: PaginationQueryParams) {
        return await this.orderService.findAllWithPagination(params);
    }


    @Get('/by-user')
    async findAllByUser(@Req() req, @Query(new ValidationPipe({ transform: true })) params: FindAllQueryParams) {
        params.id = params.id ?? req.user.id;
        return await this.orderService.findAllWithPagination(params);
    }


    @Get(':id')
    async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<Order> {
        const order = await this.orderService.findOneById(id);

        if (!order) throw new NotFoundException(`Order Not Found With Id: ${id}`);
        return order;
    }


    @Patch(':id')
    async update(@Param('id', ParseUUIDPipe) id: string, @Body() order: OrderUpdateDto) {

        const { numberOfAffectedRows, updatedOrder } = await this.orderService.update(id, order);

        // if the number of row affected is zero, 
        // it means the post doesn't exist in our db
        if (numberOfAffectedRows === 0) {
            throw new NotFoundException(`Order Not Found With Id:${id}`);
        }

        // return the updated post
        return updatedOrder;
    }


    @Delete(':id')
    async delete(@Param('id', ParseUUIDPipe) id: string, @Res() res: ExpResponse) {
        const count = await this.orderService.delete(id);

        if (count) return res.status(200).json({ message: "Order deleted successfully", delete_count: count });
        else return res.status(400).json({ message: "Order Delete Failed" });
    }


    @Post()
    async create(@Body() order: OrderCreateDto, @Request() req): Promise<Order> {
        const user_id = req.body.user_id ?? req.user.id;
        order['user_id'] = user_id;

        return await this.orderService.create(order);
    }
}
