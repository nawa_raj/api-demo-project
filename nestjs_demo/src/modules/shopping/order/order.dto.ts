import { Type } from "class-transformer";
import { IsBoolean, IsNotEmpty, IsOptional, IsArray, IsUUID, ValidateNested } from "class-validator";
import { OrderItemBaseDto } from "../order-item/order-item.dto";



export class OrderCreateDto {
    @IsNotEmpty()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => OrderItemBaseDto)
    order_items: Array<OrderItemBaseDto>;

    @IsOptional()
    @IsBoolean()
    status?: boolean;

    @IsOptional()
    @IsUUID()
    user_id?: string;
}


export class OrderUpdateDto {
    @IsNotEmpty()
    @IsBoolean()
    status: boolean;
}