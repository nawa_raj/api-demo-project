import { IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID, Min } from "class-validator";


export class OrderItemBaseDto {
    @IsNotEmpty()
    @IsString()
    readonly name: string;

    @IsNotEmpty()
    readonly image_url: string;

    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    readonly quantity: number;

    @IsNotEmpty()
    @IsNumber()
    @Min(0.01, { message: 'Single price must be greater than 0' })
    readonly single_price: number;

    @IsNotEmpty()
    @IsNumber()
    @Min(0.01, { message: 'Total price must be greater than 0' })
    readonly total_price: number;
}


export class OrderItemCreateDto extends OrderItemBaseDto {
    @IsNotEmpty()
    @IsUUID()
    readonly order_id: string;
}


export class OrderItemUpdateDto extends OrderItemCreateDto {
    @IsOptional()
    readonly name: string;

    @IsOptional()
    readonly image_url: string;

    @IsOptional()
    readonly quantity: number;

    @IsOptional()
    readonly single_price: number;

    @IsOptional()
    readonly total_price: number;

    @IsOptional()
    readonly order_id: string;
}