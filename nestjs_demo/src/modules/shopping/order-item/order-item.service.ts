/* eslint-disable no-unused-vars */
import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { REPOSITORY_KEYS } from 'src/constants/keys';
import Order from 'src/db/models/shopping/Order';
import OrderItem from 'src/db/models/shopping/OrderItem';
import { OrderItemCreateDto, OrderItemUpdateDto } from './order-item.dto';
import { ResponseDataWithPaginationType } from 'src/interfaces/response.interface';
import { PaginationPropsType } from 'src/interfaces/base.interface';



@Injectable()
export class OrderItemService {
    constructor(
        @Inject(REPOSITORY_KEYS.ORDER_ITEM_REPOSITORY) private readonly orderItemRepository: typeof OrderItem,
        @Inject(REPOSITORY_KEYS.ORDER_REPOSITORY) private readonly orderRepository: typeof Order,
    ) { }


    async findAll(order_id?: string): Promise<OrderItem[]> {
        if (order_id) {
            const orderItems = await this.orderItemRepository.findAll({ where: { order_id } });
            return orderItems

        } else {
            const orderItems = await this.orderItemRepository.findAll();
            return orderItems
        }
    }


    async findAllWithPagination({ id: order_id, page, perPage }: PaginationPropsType & { id?: string }): Promise<ResponseDataWithPaginationType<OrderItem[]>> {
        const currentPage = page || 1;
        const limit = perPage || 10;
        const offset = (currentPage - 1) * limit;

        const data: { rows: OrderItem[], count: number } = { rows: [], count: 0 }

        if (order_id) {
            const { count, rows } = await this.orderItemRepository.findAndCountAll({
                where: { order_id },
                offset,
                limit
            });
            data.count = count, data.rows = rows;

        } else {
            const { count: count1, rows: rows1 } = await this.orderItemRepository.findAndCountAll({
                offset,
                limit
            });
            data.count = count1; data.rows = rows1;
        }

        const totalPages = Math.ceil(data.count / limit);

        return {
            total_pages: totalPages,
            page: currentPage,
            per_page: limit,
            data: data.rows,
        }
    }


    async findOneById(id: string): Promise<OrderItem> {
        const orderItem = await this.orderItemRepository.findByPk(id);
        return orderItem
    }


    async create(body: OrderItemCreateDto): Promise<OrderItem> {
        const order = await this.orderRepository.findByPk(body.order_id);

        if (order) {
            const orderTotal = order.total_price + body.total_price;
            const orderTotalItems = order.total_quantity + body.quantity;

            const newOrderItem = await order.$create<OrderItem>("order_item", body);
            await order.update({ total_price: orderTotal, total_quantity: orderTotalItems });

            return newOrderItem;

        } else throw new NotFoundException(`Order not found with id: ${body.order_id}`)
    }


    async update(id: string, data: OrderItemUpdateDto): Promise<OrderItem> {
        const orderItem = await this.orderItemRepository.findByPk(id);

        if (orderItem) {
            const order = await this.orderRepository.findByPk(orderItem.order_id);

            if (order) {

                // CALCULATING ORDER TOTAL PRICE AND QUENTITY...
                const orderTotal = ((order.total_price - orderItem.total_price) + data.total_price);
                const orderTotalQty = ((order.total_quantity - orderItem.quantity) + data.quantity);

                const updatedOrderItem = await orderItem.update(data);
                await order.update({ total_price: orderTotal, total_quantity: orderTotalQty });

                return updatedOrderItem;

            } else throw new NotFoundException(`Order not found with id: ${orderItem.order_id}`);

        } else throw new NotFoundException(`OrderItem not found with id: ${id}`);
    }


    async delete(id: string): Promise<number> {
        const orderItem = await this.orderItemRepository.findByPk(id);

        if (orderItem) {
            const order = await this.orderRepository.findByPk(orderItem.order_id);

            if (order) {

                // CALCULATING ORDER TOTAL PRICE AND QUENTITY...
                const orderTotal = (order.total_price - orderItem.total_price);
                const orderTotalQty = (order.total_quantity - orderItem.quantity);

                await orderItem.destroy();
                await order.update({ total_price: orderTotal, total_quantity: orderTotalQty });

                return 1;

            } else throw new NotFoundException(`Order not found with id: ${orderItem.order_id}`);

        } else throw new NotFoundException(`OrderItem not found with id: ${id}`);
    }
}
