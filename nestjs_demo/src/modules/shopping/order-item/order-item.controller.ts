/* eslint-disable no-unused-vars */
import {
    Body,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    ParseUUIDPipe,
    Patch,
    Post,
    Query,
    Res,
    ValidationPipe
} from '@nestjs/common';
import { IsOptional, IsUUID } from 'class-validator';
import { Response as ExpResponse } from 'express';
import OrderItem from 'src/db/models/shopping/OrderItem';
import { PaginationQueryParams } from 'src/services/validation/request';
import { OrderItemCreateDto, OrderItemUpdateDto } from './order-item.dto';
import { OrderItemService } from './order-item.service';



class FindAllQueryParams extends PaginationQueryParams {
    @IsOptional()
    @IsUUID()
    id?: string;
}



@Controller('order-items')
export class OrderItemController {
    constructor(private readonly orderItemService: OrderItemService) { }


    @Get()
    async findAll(@Query(new ValidationPipe({ transform: true })) params: PaginationQueryParams) {
        return await this.orderItemService.findAllWithPagination(params);
    }

    // @Get()
    // async findAll(): Promise<OrderItem[]> {
    //     return await this.orderItemService.findAll();
    // }


    @Get('/by-order')
    async findAllByUser(@Query(new ValidationPipe({ transform: true })) params: FindAllQueryParams) {
        return await this.orderItemService.findAllWithPagination(params);
    }


    @Get(':id')
    async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<OrderItem> {
        const order = await this.orderItemService.findOneById(id);

        if (!order) throw new NotFoundException(`Order Not Found With Id: ${id}`);
        return order;
    }


    @Patch(':id')
    async update(@Param('id', ParseUUIDPipe) id: string, @Body() data: OrderItemUpdateDto) {
        return await this.orderItemService.update(id, data);
    }


    @Delete(':id')
    async delete(@Param('id', ParseUUIDPipe) id: string, @Res() res: ExpResponse) {
        const count = await this.orderItemService.delete(id);

        if (count) return res.status(200).json({ message: "Order deleted successfully", delete_count: count });
        else return res.status(400).json({ message: "Order Delete Failed" });
    }


    @Post()
    async create(@Body() body: OrderItemCreateDto): Promise<OrderItem> {
        return await this.orderItemService.create(body);
    }
}
