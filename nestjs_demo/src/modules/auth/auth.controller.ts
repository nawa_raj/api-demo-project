/* eslint-disable no-unused-vars */
import { Controller, UseGuards, Post, Body, Request, Get, Query, BadRequestException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { UserDto } from './user/dto';
import { DoesUserExist } from './guards/doesUserExist.guard';
import { SkipAuth } from './guards/auth.guard';



@Controller('auth')
@SkipAuth()
export class AuthController {
    constructor(private authService: AuthService) { }

    @Get('email-verification')
    async emailVerification(@Query('token') token: string) {
        if (token) {
            const data = await this.authService.emailVerification(token);
            const { password, ...user } = data.user["dataValues"];

            if (user) return {
                affectedRow: data.affectedRows,
                user
            };
            else throw new BadRequestException("Token is not valid or user not found")
        }
        else throw new BadRequestException("Access token is required");
    }

    @Get('access-token')
    async getAccessToken(@Query('refreshToken') token: string) {
        if (token) {
            const data = await this.authService.getAccessTokenFromRefreshToken(token);

            if (data && data.refresh_token) return data;
            else throw new BadRequestException("Token is not valid or user not found")
        }
        else throw new BadRequestException("Refresh Access token is required");
    }


    @UseGuards(AuthGuard('local'))
    @Post('login')
    async login(@Request() req) {
        console.log("User in login controller: ")
        return await this.authService.login(req.user);
    }


    @UseGuards(DoesUserExist)
    @Post('signup')
    async signUp(@Body() user: UserDto) {
        return await this.authService.signup(user);
    }
}
