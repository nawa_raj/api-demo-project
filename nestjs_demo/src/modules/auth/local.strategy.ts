import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthService } from "./auth.service";
import { IUserRequestModel } from "src/interfaces/user.interfaces";




@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly authService: AuthService) {
        super();
    }

    async validate(username: string, password: string): Promise<IUserRequestModel> {
        console.log("user req at LocalStrategy->validate");
        const user = await this.authService.validateUser(username, password);

        if (!user) {
            throw new UnauthorizedException("Invalid User Credential")
        }

        return { id: user.id, active: user.active, email: user.email, verified: user.verified } as IUserRequestModel;
    }
}