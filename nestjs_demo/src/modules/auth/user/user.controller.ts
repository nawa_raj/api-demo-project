/* eslint-disable no-unused-vars */
import {
    Body, Controller, Delete, Get, NotFoundException,
    Param, ParseUUIDPipe, Patch, Post,
    Query, Req, Res, UseGuards, ValidationPipe,
} from '@nestjs/common';
import User from 'src/db/models/auth/User';
import { UserService } from './user.service';
import { DoesUserExist } from '../guards/doesUserExist.guard';
import { UserBaseDto, UserDto, UserPasswordUpdateDto } from './dto';
import { Response } from 'express';
import { PaginationQueryParams } from 'src/services/validation/request';
import { ForgotPasswordDto } from './dto/user.dto';




@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @UseGuards(DoesUserExist)
    @Post()
    async signUp(@Body() user: UserDto) {
        return await this.userService.signUp(user);
    }


    @Get()
    async findAll(@Query(new ValidationPipe({ transform: true })) params: PaginationQueryParams) {
        return await this.userService.findAllWithPagination(params);
    }


    @Get(':id')
    async findOneById(@Req() req: any, @Param('id', ParseUUIDPipe) id: string): Promise<User> {
        const user_id = id ?? req.user.id;
        const user = await this.userService.findOneById(user_id);

        if (!user) throw new NotFoundException(`User Not Found With Id: ${id}`);
        return user;
    }


    @Patch(':id')
    async updateUser(@Param('id', ParseUUIDPipe) id: string, @Body() data: UserBaseDto) {
        const user = await this.userService.updateUser(id, data);
        const { password, ...rest } = user.toJSON();

        return rest;
    }


    @Delete(':id')
    async deleteUser(@Param('id', ParseUUIDPipe) id: string, @Res() res: Response) {
        const count = await this.userService.deleteUser(id);
        return res.status(200).json({ message: "User Deleted Successfully", delete_count: count });
    }


    @Post('/forgot-password')
    async forgotPassword(@Body() data: ForgotPasswordDto, @Res() res: Response) {
        const resd = await this.userService.forgotpassword(data);
        return res.status(200).json({ status: true, message: "Password Reset Email send please Check your email", detail: null });
    }


    @Post('/change-password')
    async changePassword(@Body() passData: UserPasswordUpdateDto) {
        const user = await this.userService.changePassword(passData);
        const { password, ...rest } = user.toJSON();
        return rest;
    }
}
