import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { JwtModule } from '@nestjs/jwt';
import { ENV_CONFIG } from 'src/config';
import { CustomJWTServices } from 'src/services/JWTServices';



const jwtConfig = ENV_CONFIG.jwt;

@Module({
    imports: [
        JwtModule.register({
            global: true,
            secret: jwtConfig.JWT_SECRET,
            signOptions: {
                expiresIn: jwtConfig.JWT_TOKEN_TTL,
                issuer: jwtConfig.JWT_ISSUER
            },
        }),
    ],
    providers: [UserService, CustomJWTServices],
    controllers: [UserController],
    exports: [UserService],
})
export class UserModule { }
