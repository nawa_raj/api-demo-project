import { IsNotEmpty, Matches, IsEmail, IsString, IsJWT } from 'class-validator';
import { passwordRegex } from 'src/constants/regex';


export class UserBaseDto {
    @IsNotEmpty()
    readonly first_name: string;

    @IsNotEmpty()
    @IsString()
    readonly last_name: string;

    @IsNotEmpty()
    @IsEmail()
    readonly email: string;
}



export class UserPasswordUpdateDto {
    @IsNotEmpty()
    @IsJWT()
    token: string;

    @IsNotEmpty()
    @Matches(passwordRegex, {
        message: 'Password must be minimum 8 characters, at least one letter, one number and one special character',
    })
    readonly old_password: string;

    @IsNotEmpty()
    @Matches(passwordRegex, {
        message: 'Password must be minimum 8 characters, at least one letter, one number and one special character',
    })
    readonly new_password: string;
}



export class UserCreateDto {
    @IsNotEmpty()
    readonly first_name: string;

    @IsNotEmpty()
    @IsString()
    readonly last_name: string;

    @IsNotEmpty()
    @IsEmail()
    readonly email: string;

    @IsNotEmpty()
    @Matches(passwordRegex, {
        message: 'Password must be minimum 8 characters, at least one letter, one number and one special character',
    })
    readonly password: string;
}


export class ForgotPasswordDto {
    @IsNotEmpty()
    @IsEmail()
    readonly email: string;

    @IsNotEmpty()
    readonly redirect_url: string;
}