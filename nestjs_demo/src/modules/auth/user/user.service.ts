/* eslint-disable no-unused-vars */
import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { REPOSITORY_KEYS } from 'src/constants/keys';
import User from 'src/db/models/auth/User';
import { UserBaseDto, UserDto, UserPasswordUpdateDto } from './dto';
import { CustomJWTServices } from 'src/services/JWTServices';
import EmailService from 'src/services/email/EmailService';
import { IUserRequestModel, IUserLoginResponseModel } from 'src/interfaces/user.interfaces';
import { ENV_CONFIG } from 'src/config';
import { PasswordServices } from 'src/services/PasswordServices';
import { ResponseDataWithPaginationType } from 'src/interfaces/response.interface';
import { PaginationPropsType } from 'src/interfaces/base.interface';
import { ForgotPasswordDto } from './dto/user.dto';
import { EmailQueService } from 'src/modules/queue/email/email.service';



const envAppConfig = ENV_CONFIG.app;


@Injectable()
export class UserService {
    // eslint-disable-next-line no-unused-vars
    protected mailer = new EmailService();

    constructor(
        @Inject(REPOSITORY_KEYS.USER_REPOSITORY) private readonly userRepository: typeof User,
        private readonly jwtService: CustomJWTServices,
        private readonly emailQueService: EmailQueService
    ) { }


    async findAll(): Promise<User[]> {
        const users = await this.userRepository.findAll({ attributes: { exclude: ['password'] } });
        return users
    }


    async findAllWithPagination({ page, perPage }: PaginationPropsType): Promise<ResponseDataWithPaginationType<User[]>> {
        const currentPage = page || 1;
        const limit = perPage || 10;
        const offset = (currentPage - 1) * limit;

        const { rows, count } = await this.userRepository.findAndCountAll({
            attributes: { exclude: ['password'] },
            offset,
            limit
        });

        const totalPages = Math.ceil(count / limit);

        return {
            total_pages: totalPages,
            page: currentPage,
            per_page: limit,
            data: rows,
        }
    }


    async create(user: UserDto): Promise<any> {
        return await this.userRepository.create(user);
    }


    async signUp(user: UserDto): Promise<IUserLoginResponseModel> {
        const newUser = await this.create(user);

        // get new user object...
        const { password, ...result } = newUser['dataValues'];
        const tokenPayload = { id: result.id, email: result.email, active: result.active, verified: result.verified } as IUserRequestModel;

        // generate token
        const token = await this.jwtService.generateAccessToken(tokenPayload);
        const refreshToken = await this.jwtService.generateRefreshToken(tokenPayload);
        const verificationLink = `${envAppConfig.APP_URL}/api/v1/auth/email-verification?token=${token}`;
        // send mail for email verification...
        // await this.mailer.sendEmailVerificationEmail({
        //     verificationLink: `${envAppConfig.APP_URL}/api/v1/auth/email-verification?token=${token}`,
        //     to: newUser.email,
        // });

        await this.emailQueService.addEmailIntoRegisterEmailQueue({ to: newUser.email, verificationLink });

        // return the user and the token
        return {
            user: result,
            refresh_token: refreshToken,
            token,
        };
    }


    async getUserForTokenValidation(email: string): Promise<User> {
        return await this.userRepository.findOne<User>({ where: { email, active: true, verified: true } });
    }


    async getUserForJWTValidation(id: string, email: string): Promise<User> {
        return await this.userRepository.findOne<User>({ where: { id, email, active: true, verified: true } });
    }


    async findOneByEmail(email: string, active?: boolean): Promise<User> {
        if (String(active) === "true" || String(active) === "false") {
            return await this.userRepository.findOne<User>({ where: { email, active } });
        }
        return await this.userRepository.findOne<User>({ where: { email }, paranoid: false });
    }


    async findOneById(id: string): Promise<User> {
        return await this.userRepository.findOne<User>({ where: { id }, attributes: { exclude: ['password'] } });
    }


    async emailVerification(id: string): Promise<{ affectedRows: number, user: User }> {
        const [numberOfAffectedRows, [updatedUser]] = await this.userRepository.update<User>(
            { active: true, verified: true },
            { where: { id: id }, returning: true }
        );

        return {
            affectedRows: numberOfAffectedRows,
            user: updatedUser
        }
    }


    async updateUser(userId: string, data: UserBaseDto): Promise<User> {
        const user = await this.userRepository.findByPk(userId);
        const updatedUser = await user.update(data);
        return updatedUser;
    }


    async deleteUser(userId: string): Promise<number> {
        const user = await this.userRepository.findByPk(userId);
        let count = 0;

        if (user) {
            await user.update({ active: false });
            await user.destroy();
            count = 1;

        } else throw new NotFoundException(`User not found with id: ${userId}`);

        return count
    }


    async forgotpassword(data: ForgotPasswordDto) {
        const user = await this.userRepository.findOne({ where: { email: data.email }, attributes: { exclude: ['password'] } });

        if (user) {
            const tokenPayload = { id: user.id, email: user.email, active: user.active, verified: user.verified } as IUserRequestModel;
            const verificationToken = await this.jwtService.generateToken({ expireTime: "5m", payload: tokenPayload });
            const verificationLink = `${data.redirect_url}?token=${verificationToken}`;

            // await this.mailer.sendPasswordResetEmail({
            //     verificationLink: `${data.redirect_url}?token=${verificationToken}`,
            //     to: data.email,
            // });
            await this.emailQueService.addEmailIntoRegisterEmailQueue({ to: user.email, verificationLink });

            return true;

        } else throw new NotFoundException(`User Doesnot exist with email ${data.email}`);
    }


    async changePassword(passData: UserPasswordUpdateDto): Promise<User> {
        const tokenData = this.jwtService.verifyToken({ token: passData.token, invalidmMsg: "Password reset token is not valid." });
        const user = await User.findByPk(tokenData.data.id);

        if (user && tokenData.isValid) {
            const match = await PasswordServices.comparePassword(passData.old_password, user.password);
            if (match) {
                const updatedUser = await user.update({ password: passData.new_password });
                return updatedUser;

            } else {
                throw new BadRequestException("Password is incorrect")
            }
        } else throw new NotFoundException(`User not found with Id: ${tokenData.data.sub} OR Token May Expire`);
    }
}