/* eslint-disable no-unused-vars */
import { Injectable, BadRequestException } from '@nestjs/common';
import { UserService } from './user/user.service';
import { UserDto } from './user/dto';
import { PasswordServices } from 'src/services/PasswordServices';
import { CustomJWTServices } from 'src/services/JWTServices';
import { IUserRequestModel } from 'src/interfaces/user.interfaces';



@Injectable()
export class AuthService {

    constructor(
        private readonly userService: UserService,
        private readonly jwtService: CustomJWTServices,
    ) { }


    public async login(user: IUserRequestModel) {
        const { password, ...userData } = (await this.userService.findOneById(user.id)).toJSON();

        const refresh_token = await this.jwtService.generateRefreshToken(user);
        const token = await this.jwtService.generateAccessToken(user);

        return { user: userData, token, refresh_token };
    }

    public async signup(user: UserDto) {
        return await this.userService.signUp(user);
    }


    async validateUser(username: string, pass: string) {
        // find if user exist with this email
        const user = await this.userService.getUserForTokenValidation(username);
        console.log("User Request In AuthService->validateUser: ", username, pass)
        if (!user) {
            return null;
        }

        // find if user password match
        const match = await PasswordServices.comparePassword(pass, user.password);
        if (!match) {
            return null;
        }

        // eslint-disable-next-line no-unused-vars
        const { password, ...result } = user['dataValues'];
        return result;
    }



    public async emailVerification(token: string) {
        const tokenData = await this.jwtService.verifyAccessToken(token);

        if (tokenData && tokenData.id) {
            return await this.userService.emailVerification(tokenData.id);
        } else null;
    }


    public async getAccessTokenFromRefreshToken(refreshToken: string) {
        const tokenData = await this.jwtService.verifyRefreshToken(refreshToken);

        if (tokenData && tokenData.id) {
            const user = await this.userService.findOneById(tokenData.id);
            const { password, ...rest } = user.toJSON();
            const tokenPayload = { id: rest.id, email: rest.email, active: rest.active, verified: rest.verified } as IUserRequestModel;

            if (user && user.active && user.verified) {
                const token = await this.jwtService.generateAccessToken(tokenPayload);

                return {
                    user: rest,
                    refresh_token: refreshToken,
                    token,
                }
            }
            else throw new BadRequestException("User Not Valid");
        } else null;
    }
}
