import { Global, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from "@nestjs/passport";
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ENV_CONFIG } from 'src/config';
import { JwtStrategy } from './jwt.strategy';
import { UserModule } from './user/user.module';
import { JwtAuthGuard } from './guards/auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { CustomJWTServices } from 'src/services/JWTServices';


const jwtConfig = ENV_CONFIG.jwt;


@Global()
@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: jwtConfig.JWT_SECRET,
      signOptions: {
        expiresIn: jwtConfig.JWT_TOKEN_TTL,
        issuer: jwtConfig.JWT_ISSUER
      },
    }),
    UserModule,
    PassportModule,
  ],

  providers: [
    AuthService,
    CustomJWTServices,
    LocalStrategy,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],

  controllers: [AuthController]
})
export class AuthModule { }
