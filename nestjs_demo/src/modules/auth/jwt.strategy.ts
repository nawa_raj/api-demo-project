/* eslint-disable no-unused-vars */
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { Strategy, ExtractJwt } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { UserService } from "./user/user.service";
import { ENV_CONFIG } from "src/config";
import { IUserRequestModel } from "src/interfaces/user.interfaces";


const jwtConfig = ENV_CONFIG.jwt;


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly userService: UserService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtConfig.JWT_SECRET,
        })
    }

    async validate(payload: IUserRequestModel) {
        // check if user in the token actually exist
        // console.log("user are in JwtStrategy -> validate method", payload);
        const user = await this.userService.getUserForJWTValidation(payload.id, payload.email);
        if (!user) {
            throw new UnauthorizedException('You are not authorized to perform the operation');
        }
        return payload;
    }
}