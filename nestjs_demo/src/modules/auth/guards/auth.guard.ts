/* eslint-disable no-unused-vars */
import { Injectable, CanActivate, ExecutionContext, SetMetadata, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { IS_SKIP_AUTH } from 'src/constants/keys';
import { Request } from "express";
import { ENV_CONFIG } from 'src/config';
import { AuthGuard } from '@nestjs/passport';
import { IUserRequestModel } from 'src/interfaces/user.interfaces';


const jwtConfig = ENV_CONFIG.jwt;


@Injectable()
export class CustomJWTAuthGuard implements CanActivate {
    constructor(
        private jwtService: JwtService,
        private reflector: Reflector
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isPublic = this.reflector.getAllAndOverride<boolean>(IS_SKIP_AUTH, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (isPublic) {
            // 💡 See this condition
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        if (!token) {
            throw new UnauthorizedException();
        }
        try {
            const payload = await this.jwtService.verifyAsync(token, {
                secret: jwtConfig.JWT_SECRET,
                issuer: jwtConfig.JWT_ISSUER,
            });
            // 💡 We're assigning the payload to the request object here
            // so that we can access it in our route handlers
            request['user'] = payload;

        } catch {
            throw new UnauthorizedException();
        }
        return true;
    }

    private extractTokenFromHeader(req: Request): string | undefined {
        const authHeader = req.headers.authorization;
        const [type, token] = authHeader?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}




@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(
        private reflector: Reflector,
        private jwtService: JwtService,
    ) {
        super();
    }

    canActivate(context: ExecutionContext) {
        const isPublic = this.reflector.getAllAndOverride<boolean>(IS_SKIP_AUTH, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (isPublic) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        const tokenData = this.validateToken(token);

        if (!token) {
            throw new UnauthorizedException('Invalid token');
        }

        if (!tokenData.active || !tokenData.verified) {
            throw new UnauthorizedException('User is not active or verified');
        }

        return super.canActivate(context);
    }

    private validateToken(token: string): IUserRequestModel {
        try {
            return this.jwtService.verify(token);

        } catch (error) {
            throw new UnauthorizedException('Invalid token');
        }
    }

    private extractTokenFromHeader(req: Request): string | undefined {
        const authHeader = req.headers.authorization;
        const [type, token] = authHeader?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}






export const SkipAuth = () => SetMetadata(IS_SKIP_AUTH, true);
