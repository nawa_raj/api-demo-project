import { Module } from '@nestjs/common';
import { EmailModule } from './email/email.module';
import { AudioModule } from './audio/audio.module';



@Module({
    imports: [EmailModule, AudioModule],
})
export class QueueModule { }
