/* eslint-disable no-unused-vars */
import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue, JobOptions } from 'bull';
import { EMAIL_QUEUE } from 'src/constants/keys';


const ProcessNames = EMAIL_QUEUE.PROCESS_NAMES;


@Injectable()
export class EmailQueService {
    constructor(@InjectQueue(EMAIL_QUEUE.NAME) private emailQueue: Queue) { }

    async addResetPasswordEmailIntoQueue({ to, verificationLink, jobOptions }: AddEmailIntoRegisterQueuePropsType) {
        const job = await this.emailQueue.add(ProcessNames.PASSWORD_RESET_EMAIL, {
            to,
            verificationLink
        }, jobOptions)

        return job;
    }


    async addEmailIntoRegisterEmailQueue({ to, verificationLink, jobOptions }: AddEmailIntoRegisterQueuePropsType) {
        const job = await this.emailQueue.add(ProcessNames.REGISTER_EMAIL, {
            to,
            verificationLink
        }, jobOptions)

        return job;
    }
}



export interface AddEmailIntoRegisterQueuePropsType {
    to: string;
    verificationLink: string;
    jobOptions?: JobOptions
}

