import { Test, TestingModule } from '@nestjs/testing';
import { EmailQueService } from './email.service';

describe('EmailService', () => {
  let service: EmailQueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmailQueService],
    }).compile();

    service = module.get<EmailQueService>(EmailQueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
