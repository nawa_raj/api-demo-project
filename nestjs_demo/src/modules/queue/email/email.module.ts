/* eslint-disable no-unused-vars */
import { Global, Module } from '@nestjs/common';
import { EMAIL_QUEUE } from 'src/constants/keys';
import { BullModule } from "@nestjs/bull";
import { EmailQueService } from './email.service';
import { join } from 'path';
import { EmailProcessor } from './email.processor';



@Global()
@Module({
    imports: [
        BullModule.registerQueue({
            name: EMAIL_QUEUE.NAME,
            // processors: [join(__dirname, 'processor.js')]
        }),
    ],

    providers: [EmailProcessor, EmailQueService],
    exports: [EmailProcessor, EmailQueService]
    // providers: [EmailQueService],
    // exports: [EmailQueService]
})
export class EmailModule { }
