import { Job, DoneCallback } from "bull";
import { EMAIL_QUEUE } from "src/constants/keys";
import EmailService from "src/services/email/EmailService";



const ProcessNames = EMAIL_QUEUE.PROCESS_NAMES;


export default function (job: Job, cb: DoneCallback) {
    console.log("data for email: ", `[${process.pid}] ${JSON.stringify(job.data)}`);

    const mailer = new EmailService(); // Instantiate the EmailService

    // Identify the job type based on job.data or job.name
    if (job.name === ProcessNames.REGISTER_EMAIL) {
        handleSendRegistrationEmail(job, cb, mailer);

    } else if (job.name === ProcessNames.PASSWORD_RESET_EMAIL) {
        sendResetEmail(job, cb, mailer);

    } else {
        // Handle unknown job types or log an error
        console.error(`Unknown job type: ${job.name}`);
        cb(new Error(`Unknown job type: ${job.name}`));
    }
}


async function handleSendRegistrationEmail(job: Job, cb: DoneCallback, mailer: EmailService) {
    try {
        // send mail for email verification...
        await mailer.sendEmailVerificationEmail({
            verificationLink: job.data.verificationLink,
            to: job.data.to,
        });
        cb();

    } catch (error) {
        console.error('Error processing REGISTER_EMAIL job:', error);
        cb(error);
    }
}

async function sendResetEmail(job: Job, cb: DoneCallback, mailer: EmailService) {
    try {
        // send mail for password reset...
        await mailer.sendPasswordResetEmail({
            verificationLink: job.data.verificationLink,
            to: job.data.to,
        });
        cb();

    } catch (error) {
        console.error('Error processing PASSWORD_RESET_EMAIL job:', error);
        cb(error);
    }
}