import { Process, Processor } from "@nestjs/bull";
import { Logger } from "@nestjs/common";
import { EMAIL_QUEUE } from "src/constants/keys";
import { Job } from "bull";
import { AddEmailIntoRegisterQueuePropsType } from "./email.service";
import EmailService from "src/services/email/EmailService";


const ProcessNames = EMAIL_QUEUE.PROCESS_NAMES;


@Processor(EMAIL_QUEUE.NAME)
export class EmailProcessor {
    private readonly logger = new Logger(EmailProcessor.name);
    protected mailer = new EmailService();

    @Process(ProcessNames.REGISTER_EMAIL)
    async handleSendRegistrationEmail(job: Job<AddEmailIntoRegisterQueuePropsType>) {
        // send mail for email verification...
        await this.mailer.sendEmailVerificationEmail({
            verificationLink: job.data.verificationLink,
            to: job.data.to,
        });
    }


    @Process(ProcessNames.PASSWORD_RESET_EMAIL)
    async sendResetEmail(job: Job<AddEmailIntoRegisterQueuePropsType>) {
        // send mail for email verification...
        await this.mailer.sendPasswordResetEmail({
            verificationLink: job.data.verificationLink,
            to: job.data.to,
        });
    }
}
