import { Module } from '@nestjs/common';
import { AudioService } from './audio.service';
import { BullModule } from "@nestjs/bull";
import { AUDIO_QUEUE } from 'src/constants/keys';
import { AudioProcessor } from './audio.processor';


@Module({
  imports: [
    BullModule.registerQueue({
      name: AUDIO_QUEUE.NAME,
    }),
  ],

  providers: [AudioService, AudioProcessor],
  exports: [AudioService, AudioProcessor]
})
export class AudioModule { }
