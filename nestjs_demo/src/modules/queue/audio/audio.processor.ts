import { Process, Processor } from "@nestjs/bull";
import { Logger } from "@nestjs/common";
import { AUDIO_QUEUE } from "src/constants/keys";
import { Job } from "bull";


const ProcessNames = AUDIO_QUEUE.PROCESS_NAMES;


@Processor(AUDIO_QUEUE.NAME)
export class AudioProcessor {
    private readonly logger = new Logger(AudioProcessor.name);

    @Process(ProcessNames.TRANSCODE)
    handleTranscode(job: Job) {
        this.logger.debug(`Start sending ${ProcessNames.TRANSCODE}...`);
        this.logger.debug(job.data);
        this.logger.debug(`${ProcessNames.TRANSCODE} Sending process Completed...`);
    }
}
