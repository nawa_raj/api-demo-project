/* eslint-disable no-unused-vars */
import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';



export function IsRequired(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: 'isRequired',
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return typeof value !== 'undefined' && value !== null && value !== '';
                },
            },
        });
    };
}
