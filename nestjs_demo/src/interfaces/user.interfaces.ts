import { ITableBaseModel } from "./base.interface";

export interface IUserRequestModel {
    id: string,
    active: boolean,
    verified: boolean,
    email: string,
}


export interface IUserModel extends ITableBaseModel {
    id: string,
    first_name: string,
    last_name: string,
    active: boolean,
    verified: boolean,
    email: string,
}


export interface IUserLoginResponseModel {
    token: string
    refresh_token: string
    user: IUserModel
}