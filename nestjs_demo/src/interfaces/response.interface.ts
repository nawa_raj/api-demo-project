export interface ResponseDataWithPaginationType<D> {
    total_pages: number;
    page: number;
    per_page: number;
    data: D;
}