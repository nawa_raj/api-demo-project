import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import ENV_CONFIG from './config/config.env';
import { ValidationPipe } from '@nestjs/common';



async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // global prefix
  app.setGlobalPrefix('api/v1');

  // GLOBAL LEVEL DATA IN REQUEST VALIDATION...
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true,
  }));


  // START SERVER IN SPECIFIC SERVER PORT...
  await app.listen(ENV_CONFIG.app.PORT, () => {
    console.log(`\n⚡️ Local server is listening ${ENV_CONFIG.app.APP_URL}\n`);
  });

}
bootstrap();
