import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import DBModule from './db/db.module';
import { AuthModule } from './modules/auth/auth.module';
import { ShoppingModule } from './modules/shopping/shopping.module';
import { BullModule } from '@nestjs/bull';
import { QueueModule } from './modules/queue/queue.module';



@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    DBModule,
    AuthModule,
    ShoppingModule,
    QueueModule,
  ],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule { }
