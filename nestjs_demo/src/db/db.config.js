const dotenv = require('dotenv');

dotenv.config();
const envData = process.env;
const dbConfig = {
    DB_DIALECT: (envData.DB_DIALECT ?? "postgres"),
    DB_USER: envData.DB_USER ?? "",
    DB_PASS: envData.DB_PASS ?? undefined,
    DB_HOST: envData.DB_HOST ?? "localhost",
    DB_PORT: envData.DB_PORT ? Number(envData.DB_PORT) : 5432,
    DB_NAME_TEST: envData.DB_NAME_TEST ?? "",
    DB_NAME_DEVELOPMENT: envData.DB_NAME_DEVELOPMENT ?? "",
    DB_NAME_PRODUCTION: envData.DB_NAME_PRODUCTION ?? "",
};


// COMMON DEFINED PROPERTY FOR ALL TABLES AT GLOBAL LABEL...
const commonDefineOption = {
    createdAt: "created_at",
    updatedAt: "updated_at",
    deletedAt: "deleted_at",
    underscored: true,
    freezeTableName: true,
    paranoid: true,
};


module.exports = {
    development: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_DEVELOPMENT,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
    test: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_TEST,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
    production: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_PRODUCTION,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
};