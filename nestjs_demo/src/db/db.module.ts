import { Global, Module } from "@nestjs/common";
import { dbProviders } from "./db.providers";
import DBModelsModule from "./models/db-models.module";


@Global()
@Module({
    imports: [DBModelsModule],
    providers: [...dbProviders],
    exports: [...dbProviders],
})
export default class DBModule { }