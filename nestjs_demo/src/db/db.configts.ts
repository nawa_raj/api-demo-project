import { IDatabaseConfig } from './interfaces/dbConfig.interface';
import { ENV_CONFIG } from '../config';
import { Model, ModelOptions } from 'sequelize';

const dbConfig = ENV_CONFIG.db;


// COMMON DEFINED PROPERTY FOR ALL TABLES AT GLOBAL LABEL...
const commonDefineOption = {
    createdAt: "created_at",
    updatedAt: "updated_at",
    deletedAt: "deleted_at",
    underscored: true,
    freezeTableName: true,
    paranoid: true,
} as ModelOptions<Model<any, any>>



export const databaseConfig: IDatabaseConfig = {
    development: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_DEVELOPMENT,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
    test: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_TEST,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
    production: {
        username: dbConfig.DB_USER,
        password: dbConfig.DB_PASS,
        database: dbConfig.DB_NAME_PRODUCTION,
        host: dbConfig.DB_HOST,
        port: dbConfig.DB_PORT,
        dialect: dbConfig.DB_DIALECT,
        define: commonDefineOption
    },
};