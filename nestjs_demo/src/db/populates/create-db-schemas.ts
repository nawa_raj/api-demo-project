/* eslint-disable no-unused-vars */
import * as dotenv from "dotenv";
import { SequelizeOptions, Sequelize } from "sequelize-typescript";
import SCHEMAS from "../db.schemas";


dotenv.config();
const NODE_ENV = process.env.NODE_ENV;


// GET dbConfig FROM db.config.js File...
const dbConfig: SequelizeOptions = require("../db.config")[NODE_ENV];
const sequelize = new Sequelize(dbConfig);


const createSchemas = async () => {
    // CHECK ALL AVAILABLE SCHEMAS IN DATABASE AFTER WE CREATE NEW SCHEMA IF NOT EXISTS...
    const schema = await sequelize.showAllSchemas({
        logging: (e) => {
            return e
        }
    });
    console.log(`\nAlready Existing Schemas: `, schema);

    const schemas = (schema as any) as Array<string>;
    const newSchemas = [...schemas];


    // CREATE SCHEMA IF NOT EXISTS...
    for (const [key, value] of Object.entries(SCHEMAS)) {
        if (!schemas.includes(value.name)) {
            await sequelize.createSchema(value.name, {
                logging: (e, t) => {
                    newSchemas.push(value.name)
                }
            });
        }
    }

    console.log(`Now All Schemas After Create New One: `, newSchemas);
    process.exit(0)
};

createSchemas();