import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from "sequelize-typescript";
import Order from "./Order";



@Table({
    schema: "shopping",
    modelName: "order_item",
    tableName: "order_item",
})
class OrderItem extends Model<OrderItem>{
    @Column({
        type: DataType.UUID,
        allowNull: false,
        defaultValue: DataType.UUIDV4,
        primaryKey: true,
    })
    id: string;


    @Column({
        type: DataType.STRING(100),
        allowNull: false,
    })
    name: string;


    @Column({
        type: DataType.TEXT,
        allowNull: false,
    })
    image_url: string;


    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    quantity: number;


    @Column({
        type: DataType.FLOAT,
        allowNull: false,
    })
    single_price: number;


    @Column({
        type: DataType.FLOAT,
        allowNull: false,
    })
    total_price: number;


    // ONE TO ONE RELATIONSHIP WITH order TABLE... 
    @ForeignKey(() => Order)
    @Column({
        type: DataType.UUID,
        allowNull: false,
    })
    order_id: string;

    @BelongsTo(() => Order)
    order: Order;
}



export default OrderItem;


// Order Item(id, uuid, order_id, name, image_path, image_url, quantity, single_price, total_price, created_at, updated_at, deleted_at)