import { Table, Model, Column, DataType, ForeignKey, BelongsTo, HasMany, } from "sequelize-typescript";
import User from "../auth/User";
import OrderItem from "./OrderItem";



@Table({
    schema: "shopping",
    modelName: "order",
    tableName: "order",
})
class Order extends Model<Order>{
    @Column({
        type: DataType.UUID,
        allowNull: false,
        defaultValue: DataType.UUIDV4,
        primaryKey: true,
    })
    id: string;


    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        unique: true,
    })
    order_number: number;


    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    total_quantity: number;


    @Column({
        type: DataType.FLOAT,
        allowNull: false,
    })
    total_price: number;


    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    })
    status: boolean;

    // ONE TO ONE RELATIONSHIP WITH USER...
    @ForeignKey(() => User)
    @Column({
        type: DataType.UUID,
        allowNull: false,
    })
    user_id: string;

    @BelongsTo(() => User)
    user: User;


    // ONE TO MANY RELATIONSHIP WITH order_item TABLE...
    @HasMany(() => OrderItem)
    order_items: OrderItem[];
}


export default Order;

// - Order(id, uuid, user_id, order_number, total_quantity, total_price, status, created_at, updated_at, deleted_at)