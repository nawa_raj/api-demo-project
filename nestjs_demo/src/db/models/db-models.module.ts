import { Global, Module } from '@nestjs/common';
import { dbModelsProviders } from './db-models.providers';


@Global()
@Module({
    providers: [...dbModelsProviders],
    exports: [...dbModelsProviders]
})
export default class DBModelsModule { }