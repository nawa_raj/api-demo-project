import { hashSync } from "bcrypt";
import { Table, Column, Model, DataType, Unique, HasMany } from "sequelize-typescript";
import Order from "../shopping/Order";



@Table({
    schema: "auth",
    modelName: "user",
    tableName: "user",
})
class User extends Model<User>{
    @Column({
        type: DataType.UUID,
        allowNull: false,
        defaultValue: DataType.UUIDV4,
        primaryKey: true,
    })
    id: string;


    @Column({
        type: DataType.STRING(100),
        allowNull: false
    })
    first_name: string;


    @Column({
        type: DataType.STRING(100),
        allowNull: false
    })
    last_name: string;


    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    })
    active: boolean;


    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    })
    verified: boolean;


    @Unique({ name: "Email", msg: "Email Already Exists" })
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            isEmail: { msg: "Email Is Not Valid" }
        }
    })
    email: string;


    @Column({
        type: DataType.STRING,
        allowNull: false,
        set(pass: string) {
            console.log("password: ", pass)
            const hashPass = hashSync(pass, 10);
            this.setDataValue('password', hashPass);
            // if (!passwordRegex.test(pass)) {
            //     throw new Error(`Password must be minimum 8 characters, at least one letter, one number and one special character`);

            // } else {
            //     const hashPass = hashSync(pass, 10);
            //     this.setDataValue('password', hashPass);
            // }
        }
    })
    password: string;

    // ONE TO MANY RELATIONSHIP WITH order TABLE...
    @HasMany(() => Order)
    orders: Order[];
}


export default User;