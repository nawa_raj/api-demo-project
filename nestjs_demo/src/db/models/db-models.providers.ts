import { REPOSITORY_KEYS } from "src/constants/keys";
import { Provider } from "@nestjs/common";
import OrderItem from "./shopping/OrderItem";
import User from "./auth/User";
import Order from "./shopping/Order";



// AGGREGATE ALL THE DATABASE MODELS AND MAKE AVAILABLE TO ALL MODULES SERVICES... 
export const dbModelsProviders = [
    { provide: REPOSITORY_KEYS.USER_REPOSITORY, useValue: User },
    { provide: REPOSITORY_KEYS.ORDER_REPOSITORY, useValue: Order },
    { provide: REPOSITORY_KEYS.ORDER_ITEM_REPOSITORY, useValue: OrderItem },

] as Provider[];