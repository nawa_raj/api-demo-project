import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { REPOSITORY_KEYS } from '../constants/keys';
import ENV_CONFIG from "../config/config.env";
import User from 'src/db/models/auth/User';
import Order from 'src/db/models/shopping/Order';
import OrderItem from 'src/db/models/shopping/OrderItem';


const NODE_ENV = ENV_CONFIG.app.NODE_ENV;


// GET dbConfig FROM db.config.js File...
const dbConfig: SequelizeOptions = require("./db.config.js")[NODE_ENV];


export const dbProviders = [{
    provide: REPOSITORY_KEYS.SEQUELIZE,

    useFactory: async () => {
        const sequelize = new Sequelize(dbConfig);
        sequelize.addModels([User, Order, OrderItem]);
        // await sequelize.sync({ force: true });  // THIS DELETE ALL EXISTING TABLES WITH ITS DATA AND CREATE ALL TABLE AGAIN
        // await sequelize.sync();  // THIS ONLY CREATE NEW TABLE IF MODEL IS PRESENT BUT NOT ALTER EXISTING TABLE. 
        await sequelize.authenticate(); // THIS ONLY VALIDATE ALL MODELS ARE VALID MODEL OR NOT INTO DB...
        return sequelize;
    },
}];