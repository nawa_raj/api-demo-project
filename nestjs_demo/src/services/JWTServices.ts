/* eslint-disable no-unused-vars */
import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ENV_CONFIG } from "src/config";
import { IUserRequestModel } from "src/interfaces/user.interfaces";


const jwtConfig = ENV_CONFIG.jwt;



@Injectable()
export class CustomJWTServices {
    constructor(private readonly jwtService: JwtService) { }

    public async generateToken({ secret, expireTime, payload }: GenerateTokenPropsType) {

        const token = await this.jwtService.signAsync(
            payload,
            {
                secret: secret ?? jwtConfig.JWT_SECRET,
                issuer: jwtConfig.JWT_ISSUER,
                expiresIn: expireTime,
            }
        );

        return token;
    }


    public verifyToken({ token, secret, invalidmMsg }: VerifyTokenPropsType) {
        const sec = secret ?? jwtConfig.JWT_SECRET;
        const returnObj = { isValid: false, data: null } as { isValid: boolean, data: any };

        const t = this.jwtService.verify(token,
            {
                secret: sec,
                issuer: jwtConfig.JWT_ISSUER
            });

        if (t) {
            returnObj.isValid = true;
            returnObj.data = t;

        } else {
            returnObj.isValid = false;
            returnObj.data = null;
        }

        return returnObj;
    }


    public async generateAccessToken(user: IUserRequestModel) {
        const token = await this.jwtService.signAsync(user);
        return token;
    }


    public async generateRefreshToken(user: IUserRequestModel) {
        const token = await this.jwtService.signAsync(user, {
            secret: jwtConfig.JWT_REFRESH_SECRET,
            issuer: jwtConfig.JWT_ISSUER,
            expiresIn: jwtConfig.JWT_REFRESH_TOKEN_TTL
        });
        return token;
    }

    async verifyRefreshToken(refreshToken: string): Promise<IUserRequestModel> {
        const tokenData = await this.jwtService.verifyAsync<IUserRequestModel>(refreshToken, {
            secret: jwtConfig.JWT_REFRESH_SECRET,
            issuer: jwtConfig.JWT_ISSUER
        });

        return tokenData
    }


    async verifyAccessToken(token: string): Promise<IUserRequestModel> {
        const tokenData = await this.jwtService.verifyAsync<IUserRequestModel>(token,
            // {
            //     secret: jwtConfig.JWT_SECRET,
            //     issuer: jwtConfig.JWT_ISSUER
            // }
        );

        return tokenData
    }

}



interface GenerateTokenPropsType {
    secret?: string,
    expireTime: number | string;
    payload: any;
}

interface VerifyTokenPropsType {
    secret?: string,
    token: string;
    invalidmMsg?: string;
}