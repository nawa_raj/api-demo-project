import { SendMailOptions, createTransport } from 'nodemailer';
import { ENV_CONFIG } from "src/config";


const mailConfig = ENV_CONFIG.mail;



export default class EmailService {
    protected config: MailConfig = {
        user: mailConfig.EMAIL,
        pass: mailConfig.EMAIL_PASS
    };

    constructor(config?: MailConfig) {
        if (config) this.config = config;
    }


    public gmailTransport = createTransport({
        service: "gmail",
        auth: {
            user: this.config.user,
            pass: this.config.pass,
        }
    });


    public sendEmailVerificationEmail = async (mailOption: ISendMailOptions) => {

        try {
            const res = await this.gmailTransport.sendMail({
                ...mailOption,
                subject: "Please Verify Your Email Address",
                from: mailConfig.EMAIL,
                html: `<body>
                            <p>To verify yor Email please click Verify Button</p>
                            <a href=${mailOption.verificationLink} style="padding:0.5rem 1rem; margin-top:0.5rem; background: blue; color: white; text-decoration: none;">
                                Verify
                            </a>
                        </body>`
            });

            if (res) {
                console.log("Email send response: ", res);

            }

            return res;
        } catch (error) {
            console.error("Error while verification email sending: ", error)
        }
    }

    public sendPasswordResetEmail = async (mailOption: ISendMailOptions) => {

        try {
            const res = await this.gmailTransport.sendMail({
                ...mailOption,
                subject: "Password Reset Email",
                from: mailConfig.EMAIL,
                html: `<body>
                            <p>To Change Password Click Following Button</p>
                            <a href=${mailOption.verificationLink} style="padding:0.5rem 1rem; margin-top:0.5rem; background: blue; color: white; text-decoration: none;">
                                Reset Password
                            </a>
                            <p style="margin-top: 0.5rem;">Note: This link is expire after 5 Minutes</p>
                        </body>`
            });

            if (res) {
                console.log("Email send response: ", res);
            }

            return res;
        } catch (error) {
            console.error("Error while verification email sending: ", error)
        }
    }
}



export const sendEmailVerificationEmailFun = async (option: ISendMailOptions) => {
    const mailTransport = createTransport({
        service: "gmail",
        auth: {
            user: mailConfig.EMAIL,
            pass: mailConfig.EMAIL_PASS
        }
    });

    try {
        const res = await mailTransport.sendMail({
            ...option,
            subject: "Please Verify Your Email Address",
            from: mailConfig.EMAIL,
            html: `<body style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
                            <p>To verify yor Email please click Verify Button</p>
                            <a href=${option.verificationLink} style="padding:0.5rem 1rem; margin-top:0.5rem; background: blue; color: white; text-decoration: none;">
                                Verify
                            </a>
                        </body>`
        });

        if (res) {
            console.log("Email send response: ", res);
        }

    } catch (error) {
        console.error("Error while verification email sending: ", error)
    }
}



interface MailConfig {
    user: string, pass: string
}

export interface ISendMailOptions extends SendMailOptions {
    verificationLink: string,
}


