import { Transform } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';


export class PaginationQueryParams {
    @IsOptional()
    @IsNumber()
    @Transform(({ value }) => parseInt(value))
    page?: number;


    @IsOptional()
    @IsNumber()
    @Transform(({ value }) => parseInt(value))
    perPage?: number;
}