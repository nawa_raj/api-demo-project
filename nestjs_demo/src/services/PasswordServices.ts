import * as bcrypt from 'bcrypt';




export class PasswordServices {

    public static async hashPassword(password: string) {
        const hash = bcrypt.hashSync(password, 10);
        return hash;
    }

    public static async comparePassword(enteredPass: string, hashedPass: string) {
        const match = await bcrypt.compare(enteredPass, hashedPass);
        return match;
    }
}