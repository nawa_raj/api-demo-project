## Installation

```bash
$ yarn install
```

## Before Run The App Follow the Step To setup App Properly

### development

$ yarn migrate:dbschema_dev &nbsp;&nbsp; -to create all required postgreSQL DB Schemas <br/>
$ yarn migrate:dbup &nbsp;&nbsp; -to create all tables for our app <br/>
$ yarn migrate:dbundo &nbsp;&nbsp; -undo all the table migration and roll back <br/>
$ yarn seed:admin &nbsp;&nbsp; -populate demo user as admin for easyness <br/>
$ yarn seed:all &nbsp;&nbsp; -populate all the required seeders file for app <br/>
$ yarn seed:undoall &nbsp;&nbsp; -Remove all populated data from db<br/>

NOTE:

1. Either Run **seed:all** OR **seed:admin** to populate demo user as admin not both command and this command is same for **Production** also.
2. user email will be admin@<DOMAIN_NAME from .env> if value is valid domain OR admin@domain.com. same for superadmin@...
3. User password for both user will be **_password@123_**

### After You Build app

$ yarn migrate:dbschema -to create all required postgreSQL DB Schemas

<br/> <br/>

## Running the app

```bash
# development
$ yarn start:dev


# build app
$ yarn build


# production mode [Note: You need to build first]
$ yarn start
$ yarn start:prod
```
