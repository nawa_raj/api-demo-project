
import { Router } from "express";
import asyncWrapper from "../../utils/asyncWrapper";



const router = Router();



router.route("/welcome").get(asyncWrapper(async (req, res, next) => {
    res.status(200).json({ status: true, message: "Welcome in API version 2", detail: null })
}));


export default router;