import express, { NextFunction, Request, Response } from "express";


// init express...
const app = express();


// aggregate routes...
app.use("/welcome", (req: Request, res: Response, next: NextFunction) => {
    return res.status(200).json({ message: "welcome message" });
});


export default app;