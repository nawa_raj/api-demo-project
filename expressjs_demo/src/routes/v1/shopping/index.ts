import express from "express";
import allOrdersRoutes from "./order";
import allOrderItemsRoutes from "./orderItem";
import AuthenticationMiddleware from "../../../middlewares/authentication";




// INIT express...
const app = express();


// AGGREGATE ALL PROTECTED SUB ROUTES OF SHOPPING DB SCHEMA...
app.use("/orders", AuthenticationMiddleware.authenticateClient, allOrdersRoutes);
app.use("/order-items", AuthenticationMiddleware.authenticateClient, allOrderItemsRoutes);


export default app;