import { Router } from "express";
import OrderItemController from "../../../controllers/v1/shopping/orderItem";



// ROUTER INSTANCE...
const router = Router();


// ALL ASSOCIATED ROUTS WITHCOMPANY INFO...
router.route("/by-order").get(OrderItemController.getOrderItemsByOrderId);
router.route("/")
    .get(OrderItemController.getAllOrderItemsWithPagination)
    .post(OrderItemController.createOrderItem);


router.route("/:id")
    .get(OrderItemController.getOrderItemById)
    .patch(OrderItemController.updateOrderItem)
    .delete(OrderItemController.deleteOrderItem);


export default router;