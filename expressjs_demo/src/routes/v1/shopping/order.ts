import { Router } from "express";
import OrderController from "../../../controllers/v1/shopping/order";



// ROUTER INSTANCE...
const router = Router();


// ALL ASSOCIATED ROUTS WITHCOMPANY INFO...
router.route("/by-user").get(OrderController.getOrdersByUserId);


router.route("/")
    .get(OrderController.getAllOrdersWithPagination)
    .post(OrderController.createOrder);


router.route("/:id")
    .get(OrderController.getOrderById)
    .patch(OrderController.updateOrder)
    .delete(OrderController.deleteOrder);


export default router;