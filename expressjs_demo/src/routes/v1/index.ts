import express from "express";
import allAuthSchemaRoutes from "./auth/index";
import welcomeRoutes from "./welcome/index";
import allShoppingSchemaRoutes from "./shopping";



// init express...
const app = express();


// COMBINE ALL ROUTES FROM THEIR FOLDERS AND PASS INTO express app instance...
app.use(
    allAuthSchemaRoutes,
    allShoppingSchemaRoutes,
    welcomeRoutes,
);


export default app;