import { Router } from "express";
import UserLoginController from "../../../controllers/v1/auth/index";



// ROUTER INSTANCE...
const router = Router();


// ALL ASSOCIATED ROUTS WITH USER AUTH...
router.route("/login").post(UserLoginController.login);
router.route("/signup").post(UserLoginController.signup);
router.route("/email-verification").get(UserLoginController.emailVerification);
router.route("/access-token").get(UserLoginController.getAccessTokenFromRefreshToken);


export default router;