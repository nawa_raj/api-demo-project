import express from "express";
import allAuthRoutes from "./auth";
import allUsersRoutes from "./user";
import AuthenticationMiddleware from "../../../middlewares/authentication";




// INIT express...
const app = express();



// AGGREGATES ALL PROTECTED Auth Schema Routes...
app.use("/users", AuthenticationMiddleware.authenticateClient, allUsersRoutes);


// AGGREGATES ALL PUBLIC Auth Schema Routes...
app.use("/auth", allAuthRoutes);



export default app;