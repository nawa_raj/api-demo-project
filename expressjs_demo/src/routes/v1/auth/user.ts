import { Router } from "express";
import UserController from "../../../controllers/v1/auth/user";


// ROUTER INSTANCE CREATE...
const router = Router();



// ALL ASSOCIATED ROUTS WITH USER...
router.route("/forgot-password").post(UserController.forgotPassword)
router.route("/change-password").post(UserController.changePassword);


router.route("/")
    .get(UserController.getUsers)
    .post(UserController.createUser);

router.route("/:id")
    .get(UserController.getUserById)
    .patch(UserController.updateUser)
    .delete(UserController.deleteUser);

export default router;