import express from "express";
import v1Routes from "./v1";
import v2Routes from "./v2";


// init express...
const app = express();


// AGGREGATES ROUTES...
app.use("/v1", v1Routes);
app.use("/v2", v2Routes);


export default app;