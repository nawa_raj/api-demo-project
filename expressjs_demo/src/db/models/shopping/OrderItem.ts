import {
    BelongsToCreateAssociationMixin,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    CreationOptional,
    ForeignKey, InferAttributes, InferCreationAttributes,
    Model, Sequelize,
    DataTypes as SequelizeDataTypes
} from 'sequelize';
import { Order } from './Order';



export class OrderItem extends Model<InferAttributes<OrderItem>, InferCreationAttributes<OrderItem>> {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    declare id: CreationOptional<string>;
    declare name: string;
    declare image_url: string;
    declare quantity: number;
    declare single_price: number;
    declare total_price: number;

    declare order_id: ForeignKey<string>;

    declare created_at: CreationOptional<Date>;
    declare updated_at: CreationOptional<Date>;
    declare deleted_at: CreationOptional<Date> | null;

    static associate(models: any) {
        // define association here

        // Many TO One RELATIONSHIP WITH Order MODEL...
        OrderItem.belongsTo(models.order);
    }

    // ==============================|| ASSOCIATIONS METHODS ||==============================//

    // Methods TO ACCESS Order Model WITH Many-to-One relationship...
    declare getOrder: BelongsToGetAssociationMixin<Order>;
    declare setOrder: BelongsToSetAssociationMixin<Order, Order['id']>;
    declare createOrder: BelongsToCreateAssociationMixin<Order>;
}


export default function (sequelize: Sequelize, DataTypes: typeof SequelizeDataTypes): typeof OrderItem {

    OrderItem.init({
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        image_url: {
            type: DataTypes.TEXT,
            allowNull: false,
        },

        single_price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },

        total_price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },

        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        order_id: {
            type: DataTypes.UUID,
            references: {
                model: "order",
                key: "id"
            }
        },

        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
        deleted_at: {
            type: DataTypes.DATE,
            allowNull: true,
        },

    }, {
        sequelize,
        schema: "shopping",
        modelName: 'order_item',
        tableName: 'order_item',
    });


    return OrderItem;
};


export type ProductType = typeof OrderItem;