import {
    BelongsToCreateAssociationMixin,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    CreationOptional,
    ForeignKey, HasManyAddAssociationMixin,
    HasManyAddAssociationsMixin, HasManyCountAssociationsMixin, HasManyCreateAssociationMixin,
    HasManyGetAssociationsMixin, HasManyHasAssociationMixin, HasManyHasAssociationsMixin,
    HasManyRemoveAssociationMixin, HasManyRemoveAssociationsMixin, HasManySetAssociationsMixin,
    InferAttributes,
    InferCreationAttributes, Model, Sequelize,
    DataTypes as SequelizeDataTypes
} from 'sequelize';
import { User } from '../auth/User';
import { OrderItem } from './OrderItem';



export class Order extends Model<InferAttributes<Order>, InferCreationAttributes<Order>> {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    declare id: CreationOptional<string>;
    declare order_number: string;
    declare total_quantity: number;
    declare total_price: number;
    declare status: boolean;

    declare user_id: ForeignKey<string>;

    declare created_at: CreationOptional<Date>;
    declare updated_at: CreationOptional<Date>;
    declare deleted_at: CreationOptional<Date> | null;

    static associate(models: any) {
        // define association here

        // Many TO One RELATIONSHIP WITH User MODEL...
        Order.belongsTo(models.user);


        // ONE TO MANY RELATIONSHIP WITH OrderItem MODEL...
        Order.hasMany(models.order_item, {
            sourceKey: "id",
            foreignKey: "order_id",
        });
    }

    // ==============================|| ASSOCIATIONS METHODS ||==============================//

    // Methods TO ACCESS User Model WITH Many-to-One relationship...
    declare getUser: BelongsToGetAssociationMixin<User>;
    declare setUser: BelongsToSetAssociationMixin<User, User['id']>;
    declare createUser: BelongsToCreateAssociationMixin<User>;

    // Methods TO ACCESS OrderItem Model WITH One-to-Many relationship...
    declare getOrder_items: HasManyGetAssociationsMixin<OrderItem>;
    declare addOrder_item: HasManyAddAssociationMixin<OrderItem, OrderItem['id']>;
    declare addOrder_items: HasManyAddAssociationsMixin<OrderItem, OrderItem["id"]>;
    declare setOrder_items: HasManySetAssociationsMixin<OrderItem, OrderItem["id"]>;
    declare removeOrder_item: HasManyRemoveAssociationMixin<OrderItem, OrderItem['id']>;
    declare removeOrder_items: HasManyRemoveAssociationsMixin<OrderItem, OrderItem['id']>;
    declare hasOrder_item: HasManyHasAssociationMixin<OrderItem, OrderItem['id']>;
    declare hasOrder_items: HasManyHasAssociationsMixin<OrderItem, OrderItem['id']>;
    declare countOrder_items: HasManyCountAssociationsMixin;
    declare createOrder_item: HasManyCreateAssociationMixin<OrderItem>;
}


export default function (sequelize: Sequelize, DataTypes: typeof SequelizeDataTypes): typeof Order {

    Order.init({
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
        },

        order_number: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },

        total_quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        total_price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },

        status: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },

        user_id: {
            type: DataTypes.UUID,
            references: {
                model: "user",
                key: "id"
            }
        },

        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
        deleted_at: {
            type: DataTypes.DATE,
            allowNull: true,
        },

    }, {
        sequelize,
        schema: "shopping",
        modelName: 'order',
        tableName: 'order',
    });


    return Order;
};


export type ProductType = typeof Order;