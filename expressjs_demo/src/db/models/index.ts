import * as path from 'path';
import { Sequelize, DataTypes } from 'sequelize';
import * as process from 'process';
import { ENV_CONFIG } from "../../config";
import getDirectoryFilePath from '../../utils/handle-file/getFilePath';


const basename: string = path.basename(__filename);
const config: any = require("../db.config")[ENV_CONFIG.app.NODE_ENV];
const db: any = {};

let sequelize: Sequelize;


if (config.use_env_variable) {
  sequelize = new Sequelize(String(process.env[config.use_env_variable] ?? ""), config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}


const allFiles = getDirectoryFilePath[ENV_CONFIG.app.NODE_ENV](__dirname, basename);


allFiles.forEach((file: string) => {
  const model = require(file).default(sequelize, DataTypes);
  db[model.name] = model;
});

Object.keys(db).forEach((modelName: string) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});


db.sequelize = sequelize;
db.Sequelize = Sequelize;



export default db;


const dbConnection = async () => {
  try {
    console.log("\nDatabase Details: ", config, "\n");
    await db.sequelize.authenticate();
    // console.log("db.sequelize.models: ", db.sequelize.models)

    // await sequelize.sync().then((e) => {
    //   console.log("database sync is completed with: ", e);
    // }).catch((error) => {
    //   console.log("database sync error: ", error);
    // });

    return { message: "Successfully Connected", status: true, error: undefined };

  } catch (error) {
    return { message: "Unable to connect to the database", status: false, error };
  }
};

export { sequelize, dbConnection };