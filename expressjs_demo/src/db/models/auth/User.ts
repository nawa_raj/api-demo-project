import { hashSync } from 'bcryptjs';
import {
  CreationOptional, DataTypes as DataTypess,
  HasManyAddAssociationMixin, HasManyAddAssociationsMixin, HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin, HasManyGetAssociationsMixin, HasManyHasAssociationMixin,
  HasManyHasAssociationsMixin, HasManyRemoveAssociationMixin, HasManyRemoveAssociationsMixin,
  HasManySetAssociationsMixin, InferAttributes, InferCreationAttributes, Model, Sequelize
} from 'sequelize';
import { USER_SCOPE_KEYS } from '../../../constants/keys/scopeKeys';
import { Order } from '../shopping/Order';



// const AuthGroupp = db.AuthGroup as AuthGroup;

export class User extends Model<InferAttributes<User>, InferCreationAttributes<User>> {
  declare id: CreationOptional<string>;
  declare first_name: string;
  declare last_name: string;
  declare email: string;
  declare password: string;
  declare active: boolean;
  declare verified: boolean;

  declare created_at: CreationOptional<Date>;
  declare updated_at: CreationOptional<Date>;
  declare deleted_at: CreationOptional<Date> | null;


  fullName(): string {
    return [this.first_name, this.last_name].join(' ');
  };

  static associate(models: any) {
    // define association here

    // ONE TO MANY RELATIONSHIP WITH Order MODEL...
    User.hasMany(models.order, {
      sourceKey: "id",
      foreignKey: "user_id",
    });
  }


  // ==============================|| ASSOCIATIONS METHODS ||==============================//


  // Methods TO ACCESS Order Model WITH One-to-Many relationship...
  declare getOrders: HasManyGetAssociationsMixin<Order>;
  declare addOrder: HasManyAddAssociationMixin<Order, Order['id']>;
  declare addOrders: HasManyAddAssociationsMixin<Order, Order["id"]>;
  declare setOrders: HasManySetAssociationsMixin<Order, Order["id"]>;
  declare removeOrder: HasManyRemoveAssociationMixin<Order, Order['id']>;
  declare removeOrders: HasManyRemoveAssociationsMixin<Order, Order['id']>;
  declare hasOrder: HasManyHasAssociationMixin<Order, Order['id']>;
  declare hasOrders: HasManyHasAssociationsMixin<Order, Order['id']>;
  declare countOrders: HasManyCountAssociationsMixin;
  declare createOrder: HasManyCreateAssociationMixin<Order>;
}


export default function (sequelize: Sequelize, DataTypes: typeof DataTypess): typeof User {
  User.init({
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },

    first_name: {
      type: DataTypes.STRING,
      allowNull: false
    },

    last_name: {
      type: DataTypes.STRING,
      allowNull: false
    },

    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: { msg: "invalid email, please provide valid email address." }
      },
    },

    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set(value: string) {
        const pass = hashSync(value, 10);
        this.setDataValue("password", pass)
      },
    },

    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },

    verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },


    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },

  }, {
    sequelize,
    schema: "auth",
    modelName: "user",
    tableName: 'user',

    defaultScope: { where: { active: true, verified: true } },
    scopes: {
      [`${USER_SCOPE_KEYS.getUser}`]: {
        attributes: { exclude: ['password'] }
      },
    }
  });


  return User;
};


export type UserModelType = typeof User;
