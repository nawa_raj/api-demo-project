/* eslint-disable no-unused-vars */
'use strict';
const dotenv = require('dotenv');
const { v4: uuidv4 } = require('uuid');
const { hashSync } = require("bcryptjs");


const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


dotenv.config();
const domainName = process.env.APP_DOMAIN;
const isValidEmail = emailRegex.test(`admin@${domainName}`);
const hassPass = hashSync("password@123", 10);


/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert({ schema: 'auth', tableName: 'user' }, [
      {
        id: uuidv4(),
        first_name: "Admin",
        last_name: 'User',
        active: true,
        verified: true,
        email: isValidEmail ? `admin@${domainName}` : "admin@domain.com",
        password: hassPass,
        created_at: new Date(),
        updated_at: new Date(),
        deleted_at: null
      },
      {
        id: uuidv4(),
        first_name: "Super Admin",
        last_name: 'User',
        active: true,
        verified: true,
        email: isValidEmail ? `superadmin@${domainName}` : "superadmin@domain.com",
        password: hassPass,
        created_at: new Date(),
        updated_at: new Date(),
        deleted_at: null
      }
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return await queryInterface.bulkDelete('auth.user', null, {});
  }
};
