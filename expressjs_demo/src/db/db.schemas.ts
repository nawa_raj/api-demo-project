

// FOLLOWING VARIABLE HOLDS ALL SCHEMA AND THEIR ASSOCIATED TABLES LISTS...
const SCHEMAS = {
    auth: {
        name: 'auth',
        tables: {
            // authGroup: "auth_group",
            // authPermission: "auth_permission",
            // authGroupPermission: "auth_group_permission",
            user: "user",
            // userAuthGroup: "user_auth_group",
            // userAuthPermission: "user_auth_permission",
            // dbModel: "db_model",
        },
    },

    shopping: {
        name: "shopping",
        tables: {
            orderItem: "order_item",
            order: "order",
        },
    },

    public: {
        name: 'public',
        tables: {
            companyInfo: "company_info",
            // contactUsForm: "contact_us_form",
            // imageFolder: "image_folder",
            // image: "image",
            // siteVisitRequest: "site_visit_request",
        },
    },
};

export default SCHEMAS;