import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { Order } from "../../../db/models/shopping/Order";
import { OrderItem } from "../../../db/models/shopping/OrderItem";
import { CustomResponseType } from "../../../interfaces/apiResponse";
import { createNewOrderItemSchema } from "../../../services/validation/shopping/order";
import asyncWrapper from "../../../utils/asyncWrapper";
import { paginationQueryWithUserIdSchema } from "../../../services/validation/query";





export default class OrderItemController {

    // GET ALL ORDER ITEMS...
    public static getAllOrderItems = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const orderItems = await OrderItem.findAll();

        res.status(StatusCodes.OK).json({
            status: true,
            message: "OrderItem Data Fetch Successfully",
            detail: orderItems
        })
    });


    // GET SINGLE ORDER ITEM BY ID...
    public static getOrderItemById = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        if (id) {
            const orderItem = await OrderItem.findByPk(id as string);

            if (orderItem) {
                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "OrderItem Fetch Successfully",
                    detail: orderItem
                })
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `OrderItem Not Found with id: ${id}`,
                detail: orderItem
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Please Provide Valid OrderItem Id",
            detail: { id: id, body: req.body }
        })
    });


    // CREATE SINGLE ORDER ITEM...
    public static createOrderItem = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { order_id, total_price, quantity } = req.body;

        const validate = createNewOrderItemSchema.validateSync(req.body);

        if (validate) {
            const order = await Order.findByPk(order_id);

            if (order) {
                const orderTotal = order.total_price + Number(total_price);
                const orderTotalItems = order.total_quantity + Number(quantity);

                const newOrderItem = await order.createOrder_item(req.body);
                await order.update({ total_price: orderTotal, total_quantity: orderTotalItems });

                return res.status(StatusCodes.CREATED).json({
                    status: true,
                    message: `Order Item Created Successfully`,
                    detail: newOrderItem
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `Order not found with id ${order_id}`,
                detail: null
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Data Validation Failed",
            detail: null
        })
    });


    // UPDATE SINGLE ORDER ITEM BY ID...
    public static updateOrderItem = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;
        const { total_price, quantity } = req.body;

        if (id) {
            const orderItem = await OrderItem.findByPk(id);

            if (orderItem) {
                const order = await Order.findByPk(orderItem.order_id);

                if (order) {

                    // CALCULATING ORDER TOTAL PRICE AND QUENTITY...
                    const orderTotal = ((order.total_price - orderItem.total_price) + Number(total_price));
                    const orderTotalQty = ((order.total_quantity - orderItem.quantity) + Number(quantity));

                    const updatedOrderItem = await orderItem.update(req.body);
                    await order.update({ total_price: orderTotal, total_quantity: orderTotalQty });

                    return res.status(StatusCodes.OK).json({
                        status: true,
                        message: "Order Item Updated Successfully",
                        detail: updatedOrderItem
                    });
                }

                return res.status(StatusCodes.NOT_FOUND).json({
                    status: true,
                    message: `Order Not Found with id: ${orderItem.order_id}`,
                    detail: null
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: true,
                message: `Order Item Not Found with id: ${id}`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `Order Item Id Required to update record`,
            detail: null
        })
    });


    // DELETE SINGLE ORDER ITEM BY ID...
    public static deleteOrderItem = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        if (id) {
            const delRes = await OrderItem.destroy({ where: { id } })

            res.status(StatusCodes.OK).json({
                status: true,
                message: "Order Item Deleted successfully",
                detail: null
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `OrderItem Id is required for delete a record`,
            detail: null
        })
    });



    // ------------------------ EXTRA METHODS FOR ORDER ITEMS -----------------------------//

    // GET ALL ORDER ITEMS OF ORDER...
    public static getOrderItemsByOrderId = asyncWrapper(async (req: Request, res: Response<CustomResponseType>, next) => {
        const { id } = req.query;

        if (id && String(id).length) {
            // const order = await Order.findByPk(String(id));

            // if (order) {
            //     const orderItems = await order.getOrder_items();

            //     return res.status(StatusCodes.OK).json({
            //         status: true,
            //         message: "User Orders Fetch Successfully",
            //         detail: orderItems
            //     })
            // }

            // return res.status(StatusCodes.NOT_FOUND).json({
            //     status: false,
            //     message: `Order Not Found with id: ${id}`,
            //     detail: null
            // })

            return this.getAllOrderItemsWithPagination(req, res, next);
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Please Provide Valid Order Id",
            detail: { id: id, body: req.body }
        })
    });


    // GET ALL ORDERS...
    public static getAllOrderItemsWithPagination = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { perPage, page, id: order_id } = req.query;

        const validatedData = paginationQueryWithUserIdSchema.validateSync({ perPage, page, id: order_id });

        if (validatedData) {
            const currentPage = page ? Number(page) : 1;
            const limit = perPage ? Number(perPage) : 10;
            const offset = (currentPage - 1) * limit;
            let data: { count: number, rows: OrderItem[] } = { count: 0, rows: [] };
            let totalPages = 1;

            if (order_id) {
                data = await OrderItem.findAndCountAll({ where: { order_id: validatedData.id }, offset, limit });
                totalPages = Math.ceil(data.count / limit);
            } else {
                data = await OrderItem.findAndCountAll({ offset, limit });
                totalPages = Math.ceil(data.count / limit);
            }

            return res.status(StatusCodes.OK).json({
                status: true,
                message: "Order Item Data Fetch Successfully",
                detail: {
                    total_pages: totalPages,
                    page: currentPage,
                    per_page: limit,
                    data: data.rows,
                }
            });
        }

    });
}
