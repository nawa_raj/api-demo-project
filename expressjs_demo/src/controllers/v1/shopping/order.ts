import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import * as Yup from "yup";
import { User } from "../../../db/models/auth/User";
import { Order } from "../../../db/models/shopping/Order";
import { OrderItem } from "../../../db/models/shopping/OrderItem";
import { CustomResponseType } from "../../../interfaces/apiResponse";
import { createNewOrderSchema } from "../../../services/validation/shopping/order";
import asyncWrapper from "../../../utils/asyncWrapper";
import { generateOrderNumber } from "../../../utils/common";
import { paginationQueryWithUserIdSchema } from "../../../services/validation/query";




export default class OrderController {

    // GET ALL ORDERS...
    public static getAllOrders = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const orders = await Order.findAll({ include: [{ model: OrderItem }] });

        return res.status(StatusCodes.OK).json({
            status: true,
            message: "Order Data Fetch Successfully",
            detail: orders
        })
    });


    // GET SINGLE ORDER BY ID...
    public static getOrderById = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        if (id) {
            const order = await Order.findByPk(id as string, { include: [OrderItem] });

            if (order) {
                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "Order Fetch Successfully",
                    detail: order
                })
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `Order Not Found with id: ${id}`,
                detail: order
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Please Provide Valid Order Id",
            detail: { id: id, body: req.body }
        })
    });


    // CREATE SINGLE ORDER...
    public static createOrder = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { order_items, status, user_id, } = req.body;
        type OrderItemType = Yup.InferType<typeof createNewOrderSchema>['order_items'];

        const validate = createNewOrderSchema.validateSync(req.body);

        if (validate && Array.isArray(order_items) && order_items.length) {
            const userId = user_id ?? req.user.id;
            const user = await User.findByPk(userId, { attributes: { exclude: ["password"] } });

            if (user && user.id) {
                const orderItemsInReq: OrderItemType = order_items;
                let totalQty = 0;
                let totalPrice = 0;

                // FIND LAST ORDER NUMBER AND INCREMENT OF THAT AND ASSIGN TO NEW ORDER...
                const lastOrder = await Order.findOne({ order: [['created_at', 'DESC']] });
                const order_number = generateOrderNumber(lastOrder ? lastOrder.order_number + 1 : 1);

                orderItemsInReq.forEach((item) => {
                    totalQty += item.quantity;
                    totalPrice += item.total_price;
                })

                const newOrder = await Order.create({
                    user_id: user.id,
                    status: status,
                    total_price: totalPrice,
                    total_quantity: totalQty,
                    order_number,
                });

                // NOW WE NEE TO CREATE ORDER ITEMS FOR THIS ORDER...
                const refinedOrderItems = orderItemsInReq.map((item) => {
                    return { ...item, order_id: String(newOrder.id) }
                });

                const orderItems = await OrderItem.bulkCreate(refinedOrderItems);
                await newOrder.setOrder_items(orderItems)

                return res.status(StatusCodes.CREATED).json({
                    status: true,
                    message: `Order Created Successfully`,
                    detail: {
                        ...newOrder.toJSON(),
                        order_items: orderItems,
                        user,
                    }
                });
            }

            return res.status(StatusCodes.BAD_REQUEST).json({
                status: true,
                message: `User Id is Not Valid or User Not Found with Id:${user_id}`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Data Validation Failed, Array of order_items may be empty",
            detail: null
        })
    });


    // UPDATE SINGLE ORDER BY ID...
    public static updateOrder = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;
        const { status } = req.body;

        if (id) {
            const order = await Order.findByPk(id, { include: [OrderItem] });

            if (order && order.id && String(status)) {
                const updatedOrder = await order.update({ status: status });

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "Order Updated Successfully",
                    detail: updatedOrder
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `Order Not Found with id: ${id} OR completed Field Not Found`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `Order Id Required to update record`,
            detail: null
        })
    });


    // DELETE SINGLE ORDER BY ID...
    public static deleteOrder = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        if (id) {
            const deleterec = await Order.destroy({ where: { id } });

            return res.status(StatusCodes.OK).json({
                status: true,
                message: `Order Deleted successfully`,
                detail: deleterec
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `Order Id is required to delete a record`,
            detail: null
        })
    });


    // ------------------------- EXTERA METHODS TO HANDLE ORDER --------------------------//
    public static getOrdersByUserId = asyncWrapper(async (req: Request, res: Response<CustomResponseType>, next) => {
        const { id } = req.query;
        const user_id = id as string ?? req.user.id;

        if (user_id) {
            // const user = await User.findByPk(user_id);

            // if (user) {
            //     const orders = await user.getOrders({ include: [OrderItem] });

            //     return res.status(StatusCodes.OK).json({
            //         status: true,
            //         message: "User Orders Fetch Successfully",
            //         detail: orders
            //     })
            // }

            // return res.status(StatusCodes.NOT_FOUND).json({
            //     status: false,
            //     message: `User Not Found with id: ${user_id}`,
            //     detail: null
            // })

            return this.getAllOrdersWithPagination(req, res, next);
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Please Provide Valid User Id",
            detail: { id: user_id, body: req.body }
        })
    });


    // GET ALL ORDERS...
    public static getAllOrdersWithPagination = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { perPage, page, id: user_id } = req.query;

        const validatedData = paginationQueryWithUserIdSchema.validateSync({ perPage, page, id: user_id });

        if (validatedData) {
            const currentPage = page ? Number(page) : 1;
            const limit = perPage ? Number(perPage) : 10;
            const offset = (currentPage - 1) * limit;
            let data: { count: number, rows: Order[] } = { count: 0, rows: [] };
            let totalPages = 1;

            if (user_id) {
                data = await Order.findAndCountAll({
                    where: { user_id: validatedData.id },
                    include: [{ model: OrderItem }],
                    offset,
                    limit
                });
                totalPages = Math.ceil(data.count / limit);

            } else {
                data = await Order.findAndCountAll({ include: [{ model: OrderItem }], offset, limit });
                totalPages = Math.ceil(data.count / limit);
            }

            return res.status(StatusCodes.OK).json({
                status: true,
                message: "Order Data Fetch Successfully",
                detail: {
                    total_pages: totalPages,
                    page: currentPage,
                    per_page: limit,
                    data: data.rows,
                }
            });
        }

    });
}
