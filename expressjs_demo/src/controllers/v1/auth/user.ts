import { NextFunction, Request, Response } from "express";
import { User } from "../../../db/models/auth/User";
import { CustomResponseType } from "../../../interfaces/apiResponse";
import asyncWrapper from "../../../utils/asyncWrapper"
import { StatusCodes } from "http-status-codes";
import { USER_SCOPE_KEYS } from "../../../constants/keys/scopeKeys";
import { changePasswordSchema, userSignupSchema } from "../../../services/validation/auth/common";
import { validate as UUIDValidate } from "uuid"
import { PasswordServices } from "../../../services/PasswordServices";
import * as Yup from "yup";
import JwtToken from "../../../utils/token";
import EmailService from "../../../services/email/EmailService";
import { ENV_CONFIG } from "../../../config";


const envAppConfig = ENV_CONFIG.app;



class UserController {
    public static UserListAttributes = ['id', 'first_name', 'last_name', 'email', 'active', 'verified'];


    // GET ALL USERS...
    public static getUsers = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { perPage, page } = req.query;

        const currentPage = page ? Number(page) : 1;
        const limit = perPage ? Number(perPage) : 10;
        const offset = (currentPage - 1) * limit;

        if (page && perPage) {
            if (parseInt(String(page)) && parseInt(String(perPage))) {
                const { rows, count } = await User.scope(USER_SCOPE_KEYS.getUser).findAndCountAll({ offset, limit });
                const totalPages = Math.ceil(count / limit);

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "User Data Fetch Successfully",
                    detail: {
                        total_pages: totalPages,
                        page: currentPage,
                        per_page: limit,
                        data: rows,
                    }
                })
            }

            return res.status(StatusCodes.BAD_REQUEST).json({
                status: true,
                message: "Page and perPage are required OR must be Number",
                detail: null,
            })

        } else {
            const { rows, count } = await User.scope(USER_SCOPE_KEYS.getUser).findAndCountAll();
            const totalPages = Math.ceil(count / limit);

            return res.status(StatusCodes.OK).json({
                status: true,
                message: "User Data Fetch Successfully",
                detail: {
                    total_pages: totalPages,
                    page: currentPage,
                    per_page: limit,
                    data: rows,
                }
            })
        }
    });


    // GET USER BY ID...
    public static getUserById = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        const user = await User.scope(USER_SCOPE_KEYS.getUser).findByPk(id);

        if (user) {
            return res.status(StatusCodes.OK).json({
                status: true,
                message: "User Data Fetch Successfully",
                detail: user
            });
        }

        return res.status(StatusCodes.NOT_FOUND).json({
            status: false,
            message: `User Not Found With Id: ${id}`,
            detail: null,
        })
    });


    // CREATE A USER...
    public static createUser = asyncWrapper(async (req: Request, res: Response<CustomResponseType>, next: NextFunction) => {
        const validate = userSignupSchema.validateSync(req.body);

        if (validate) {
            const [user, created] = await User.scope(USER_SCOPE_KEYS.getUser).findOrCreate({
                where: { email: req.body.email },
                defaults: req.body
            });

            if (created) {
                return res.status(StatusCodes.CREATED).json({
                    status: true,
                    message: "User Created Successfully",
                    detail: user,
                })
            }

            return res.status(StatusCodes.BAD_REQUEST).json({
                status: true,
                message: "User Already Exist Successfully",
                detail: user,
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: 'User Signin Data Validation Failed',
            detail: null,
        })
    })


    // UPDATE SINGLE USER BY ID...
    public static updateUser = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        const reqBody = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            active: req.body.active,
            verified: req.body.verified,
        }

        if (id) {
            const user = await User.scope(USER_SCOPE_KEYS.getUser).findByPk(id);

            if (user) {
                const updatedUser = await user.update(reqBody);

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "User Data Updated Successfully",
                    detail: updatedUser
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `User Not Found with Id: ${id}`,
                detail: null
            })
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `User Id Required to update record`,
            detail: null
        })
    });


    // UPDATE SINGLE USER BY ID...
    public static deleteUser = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const { id } = req.params;

        if (id) {
            const user = await User.destroy({ where: { id } });

            return res.status(StatusCodes.OK).json({
                status: true,
                message: "User Deleted Successfully",
                detail: user
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: `User Id Required to delete a record`,
            detail: null
        })
    });


    // --------------------------------- ALL OTHER RELATED SERVICES --------------------------------//

    // CHANGE USER PASSWORD...
    public static changePassword = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const validate = changePasswordSchema.validateSync(req.body);

        if (validate) {
            const tokenData = JwtToken.verifyToken({ token: validate.token, invalidmMsg: "Password reset token is not valid." });
            const user = await User.findByPk(tokenData.data.id);

            if (user && tokenData.isValid) {
                const match = await PasswordServices.comparePassword(validate.old_password, user.password);

                if (match) {
                    const updatedUser = await user.update({ password: validate.new_password });
                    const { password, ...jdUser } = updatedUser.toJSON();

                    return res.status(StatusCodes.OK).json({
                        status: true,
                        message: "Pssword Updated Successfully",
                        detail: jdUser
                    });
                }

                return res.status(StatusCodes.BAD_REQUEST).json({
                    status: false,
                    message: `Your Password is not matched with record`,
                    detail: null
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `User not found with Id: ${tokenData.data.sub} OR Token May Expire`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Request Data Validation Failed",
            detail: null
        });
    });


    // FORGOT PASSWORD...
    public static forgotPassword = asyncWrapper(async (req: Request, res: Response<CustomResponseType>) => {
        const validate = Yup.object({
            email: Yup.string().label("Email").email().required(),
            redirect_url: Yup.string().label("Redirect URL").required(),
        }).validateSync(req.body);

        const mailer = new EmailService();

        if (validate) {
            const user = await User.findOne({ where: { email: validate.email } });

            if (user) {
                const tokenPayload = { id: user.id, email: user.email, active: user.active, verified: user.verified }
                const verificationToken = JwtToken.generateToken({ expireTime: "5m", payload: tokenPayload });

                await mailer.sendPasswordResetEmail({
                    verificationLink: `${validate.redirect_url}?token=${verificationToken}`,
                    to: validate.email,
                });

                return res.status(StatusCodes.BAD_REQUEST).json({
                    status: false,
                    message: `Password Reset Email send please Check your email`,
                    detail: null
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `User not found with email: ${req.body.email}`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Request Data Validation Failed",
            detail: null
        });
    });
}

export default UserController;
