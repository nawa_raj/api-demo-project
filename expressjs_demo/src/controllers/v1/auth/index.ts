import { compareSync } from "bcryptjs";
import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import { User } from "../../../db/models/auth/User";
import { CustomResponseType } from "../../../interfaces/apiResponse";
import { userLoginSchema, userSignupSchema } from "../../../services/validation/auth/common";
import asyncWrapper from "../../../utils/asyncWrapper";
import JwtToken from "../../../utils/token";
import EmailService from "../../../services/email/EmailService";
import { ENV_CONFIG } from "../../../config";

const envAppConfig = ENV_CONFIG.app;


export default class UserLoginController {

    // HANDLE USER LOGIN... 
    public static login = asyncWrapper(async (req, res: Response<CustomResponseType>, next) => {
        const { username: email, password } = req.body;

        // VALIDATE REQUEST BODY AND RETURN ERROR IF VALIDATION FAILED...
        const validate = userLoginSchema.validateSync(req.body);

        if (validate) {
            const user = await User.findOne({ where: { email } });

            // CHECK USER AND COMPARE USER PASSWORD IN DATABASE...
            if (user && compareSync(password, user.password)) {

                // UPDATE USER DATA INTO REQUEST STACK...
                req.user = { id: user.id, email: user.email, active: user.active, verified: user.verified };

                // GET USER AUTH TOKENS...
                const access_token = JwtToken.getAccessToken(user);
                const refresh_token = JwtToken.getRefreshToken(user);

                // remove password from user...
                const { password, ...rest } = user.toJSON();

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "Login Successfully",
                    detail: {
                        access_token,
                        refresh_token,
                        user: rest
                    }
                });
            }
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Login Data Validation Failed",
            detail: null
        });
    });


    // HANDLE USER SIGNUP...
    public static signup = asyncWrapper(async (req, res: Response<CustomResponseType>, next) => {
        const { email } = req.body;
        const mailer = new EmailService();

        // VALIDATE REQUEST BODY AND RETURN ERROR IF VALIDATION FAILED...
        const validate = userSignupSchema.validateSync(req.body);

        if (validate) {
            // FIND OR CRETA USESR...
            const findUser = await User.findOne({ where: { email }, paranoid: false });

            if (findUser) {
                await findUser.restore();
                return res.status(StatusCodes.CONFLICT).json({
                    status: false, message: "User Already Exist Please Login",
                    detail: null
                })

            } else {
                const newUser = await User.create(req.body);

                // UPDATE USER DATA INTO REQUEST STACK...
                const tokenPayload = { email: newUser.email, active: newUser.active, verified: newUser.verified }
                req.user = { ...tokenPayload, id: newUser.id };

                // GET USER AUTH TOKENS...
                const access_token = JwtToken.getAccessToken(newUser);
                const verificationToken = JwtToken.generateToken({ expireTime: "5m", payload: { ...tokenPayload, sub: newUser.id } })
                const refresh_token = JwtToken.getRefreshToken(newUser);

                const { password, ...user } = newUser.toJSON();

                await mailer.sendEmailVerificationEmail({
                    verificationLink: `${envAppConfig.APP_URL}/api/v1/auth/email-verification?token=${verificationToken}`,
                    to: newUser.email,
                });

                return res.status(StatusCodes.CREATED).json({
                    status: true,
                    message: "User Signup Successfully",
                    detail: { access_token, refresh_token, user }
                });
            }
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "User Signup Data Validation Failed",
            detail: null
        });
    });


    // HANDLE EMAIL VERIFICATION...
    public static emailVerification = asyncWrapper(async (req, res: Response<CustomResponseType>, next) => {
        const { token } = req.query;

        // VALIDATE ACCESS TOKEN WHICH IS RECEIVED FROM EMAIL LINK...
        const { isValid, userData } = JwtToken.verifyAccessToken(String(token));

        if (isValid && userData) {
            const user = await User.findByPk(userData.id);

            if (user) {
                const updatedUser = await user.update({ active: true, verified: true, deleted_at: null });
                req.user = { id: updatedUser.id, email: updatedUser.email, active: updatedUser.active, verified: updatedUser.verified };

                // GET USER AUTH TOKENS...
                const access_token = JwtToken.getAccessToken(updatedUser);
                const refresh_token = JwtToken.getRefreshToken(updatedUser);

                // remove password from user...
                const { password, ...rest } = updatedUser.toJSON();

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "Your Email Verified Successfully",
                    detail: {
                        access_token,
                        refresh_token,
                        user: rest
                    }
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `User not found with Id: ${userData.id}`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Your Link Is Expired",
            detail: null
        });
    });


    // GET ACCESS TOKEN FROM FRESH TOKEN...
    public static getAccessTokenFromRefreshToken = asyncWrapper(async (req, res: Response<CustomResponseType>, next) => {
        const { refreshToken } = req.query;

        // VALIDATE ACCESS TOKEN WHICH IS RECEIVED FROM EMAIL LINK...
        const { isValid, userData } = JwtToken.verifyRefreshToken(String(refreshToken));

        if (isValid && userData) {
            const user = await User.findByPk(userData.id);

            if (user) {
                req.user = { id: user.id, email: user.email, active: user.active, verified: user.verified };

                // GET USER AUTH TOKENS...
                const access_token = JwtToken.getAccessToken(user);
                const refresh_token = JwtToken.getRefreshToken(user);

                // remove password from user...
                const { password, ...rest } = user.toJSON();

                return res.status(StatusCodes.OK).json({
                    status: true,
                    message: "Access Token Get Successfully",
                    detail: {
                        access_token,
                        refresh_token,
                        user: rest
                    }
                });
            }

            return res.status(StatusCodes.NOT_FOUND).json({
                status: false,
                message: `User not found with Id: ${userData.id}`,
                detail: null
            });
        }

        return res.status(StatusCodes.BAD_REQUEST).json({
            status: false,
            message: "Invalid Refresh Token",
            detail: null
        });
    });
};