export const JWT_PAYLOAD_KEYS = {
    signin: {
        id: "sub",
        email: "email",
        active: "active",
        verified: "verified",
    }
}