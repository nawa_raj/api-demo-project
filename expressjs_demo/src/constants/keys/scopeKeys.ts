
// USER_SCOPE_KEYS HOLD ALL SCOPE KEYS USER MODELS... 
export const USER_SCOPE_KEYS = {
    defaultScope: "defaultScope",
    getUser: "getUser",
}




// CLIENT_SCOPE_KEYS HOLD ALL SCOPE KEYS FOR CLIENT MODEL... 
export const CLIENT_SCOPE_KEYS = {
    getLogo: "getLogo",
}