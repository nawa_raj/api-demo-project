/* 
    VALIDATE PASSWORD:
    at least 8 character 
    at least one special character
    support string or number
*/
export const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;


/*
    REGEX THAT MATCH FOLLOWING PATTERNS 
    http://localhost:3000 to http://localhost:3009
    https://localhost:3000 to https://localhost:3009
    http://127.0.0.1:3000 to http://127.0.0.1:3009
    https://127.0.0.1:3000 to https://127.0.0.1:3009
*/
export const localhostRegex = /^https?:\/\/localhost:(30([0-9]|[1-9][0-9])|127\.0\.0\.1:(30([0-9]|[1-9][0-9])))$/;


// EMAIL VALIDATION REGEX...
export const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;