import { hashSync, compare } from 'bcryptjs';



export class PasswordServices {
    public static async hashPassword(password: string) {
        const hash = hashSync(password, 10);
        return hash;
    }

    public static async comparePassword(enteredPass: string, hashedPass: string) {
        const match = await compare(enteredPass, hashedPass);
        return match;
    }
}