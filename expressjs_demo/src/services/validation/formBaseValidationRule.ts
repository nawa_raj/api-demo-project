import * as Yup from "yup";


export const formBaseSchema = Yup.object({
    id: Yup.string()
        .label("Id")
        .optional(),

    created_at: Yup.string()
        .label("Created At")
        .optional(),

    updated_at: Yup.string()
        .label("Updated At")
        .optional()

});