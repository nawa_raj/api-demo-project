import { passwordRegex } from "../../constants/regex"

export const validatePassword = (pass: any) => {
    if (pass && !String(pass).match(passwordRegex)) {
        return {
            isPass: false,
            message: "Password must be minimum 8 characters, at least one letter, one number and one special character"
        }
    } else if (pass && String(pass).match(passwordRegex)) {
        return {
            isPass: true,
            message: "passed"
        }
    } else {
        return {
            isPass: false,
            message: "failed"
        }
    }
}