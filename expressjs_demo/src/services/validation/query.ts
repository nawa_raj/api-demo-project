import * as Yup from "yup";


export const paginationQueryWithUserIdSchema = Yup.object({
    id: Yup.string()
        .label("Id")
        .uuid("Invalid Id")
        .optional(),

    page: Yup.number()
        .label("Current Page")
        .optional(),

    perPage: Yup.number()
        .label("Per page")
        .optional(),
})