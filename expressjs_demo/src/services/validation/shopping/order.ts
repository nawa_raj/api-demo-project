import * as Yup from "yup";


// ORDER CREATE CREATE REQUEST DATA BODY SCHEMA...
export const createNewOrderSchema = Yup.object({
    user_id: Yup.string()
        .uuid()
        .label("User Id")
        .optional(),

    status: Yup.string()
        .label("Order Status")
        .optional(),

    order_items: Yup.array().of(
        Yup.object().shape({
            name: Yup.string()
                .label("Product Name")
                .required(),

            image_url: Yup.string()
                .label("Image Url")
                .required(),

            quantity: Yup.number()
                .label("Product Quentity")
                .required(),

            single_price: Yup.number()
                .label("Product Single Price")
                .required(),

            total_price: Yup.number()
                .label("Product Single Price")
                .required(),

        })).label("Order item").required(),
});



// ORDER CREATE CREATE REQUEST DATA BODY SCHEMA...
export const createNewOrderItemSchema = Yup.object({
    order_id: Yup.string()
        .label("Order Id")
        .uuid()
        .required(),

    name: Yup.string()
        .label("Product Name")
        .required(),

    image_url: Yup.string()
        .label("Image Url")
        .required(),

    quantity: Yup.number()
        .label("Product Quentity")
        .required(),

    single_price: Yup.number()
        .label("Product Single Price")
        .required(),

    total_price: Yup.number()
        .label("Product Single Price")
        .required(),

});