import * as Yup from "yup"
import { passwordRegex } from "../../../constants/regex";



// LOGIN REQUEST BODY DATA VALIDATION SCHEMA...
export const userLoginSchema = Yup.object({
    username: Yup.string()
        .label("username")
        .email("Invalid username as Email")
        .required(),

    password: Yup.string()
        .label("Password")
        .matches(passwordRegex, "Password must be at least 8 characters long, 1 special character and one numeric value")
        .required()
});



// LOGIN REQUEST BODY DATA VALIDATION SCHEMA...
export const userSignupSchema = Yup.object({
    email: Yup.string()
        .label("Email Address")
        .email("Invalid Email")
        .required(),

    password: Yup.string()
        .label("Password")
        .matches(passwordRegex, "Password must be at least 8 characters long, 1 special character and one numeric value")
        .required(),

    first_name: Yup.string()
        .label("First Name")
        .required(),

    last_name: Yup.string()
        .label("Last Name")
        .required(),
});



// LOGIN REQUEST BODY DATA VALIDATION SCHEMA...
export const changePasswordSchema = Yup.object({
    token: Yup.string()
        .label("Password Change Token")
        .required(),

    old_password: Yup.string()
        .label("Old Password")
        .matches(passwordRegex, "Password must be at least 8 characters long, 1 special character and one numeric value")
        .required(),

    new_password: Yup.string()
        .label("Old Password")
        .matches(passwordRegex, "Password must be at least 8 characters long, 1 special character and one numeric value")
        .required(),
});