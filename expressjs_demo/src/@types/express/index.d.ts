// import { customResponse } from "../../interfaces/apiResponse";
import { IUserInRequestModel } from "../../interfaces/user.interfaces";


declare global {
  namespace Express {
    interface Request {
      user: IUserInRequestModel;
    }

    // interface Response<customResponse> { };
  }
}
