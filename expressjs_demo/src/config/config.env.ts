import * as dotenv from "dotenv";
import { APP_ENV_KEYS } from "../constants/keys/common.keys";
import { Options } from "sequelize";



// INIT dotenv...
dotenv.config();
const envData = process.env;
const environment = process.env.NODE_ENV ?? APP_ENV_KEYS.DEVELOPMENT;
const isDev = String(environment).toLowerCase() === String(APP_ENV_KEYS.DEVELOPMENT).toLowerCase();
const app_url = envData.APP_URL ?? "http://localhost";
const app_port = envData.PORT || 8000;


const ENV_CONFIG = {
    app: {
        APP_NAME: envData.APP_NAME,
        APP_DOMAIN: envData.APP_DOMAIN,
        APP_URL: `${app_url}:${app_port}`,
        PORT: app_port,
        NODE_ENV: environment,
        IS_DEV: isDev,
        ALLOWED_ORIGINS: envData.ALLOWED_ORIGINS ?? `http://localhost:3000,http://localhost:3001`,
    },
    db: {
        DB_DIALECT: (envData.DB_DIALECT ?? "postgres") as Options['dialect'],
        DB_USER: envData.DB_USER ?? "",
        DB_PASS: envData.DB_PASS ?? undefined,
        DB_HOST: envData.DB_HOST ?? "localhost",
        DB_PORT: envData.DB_PORT ? Number(envData.DB_PORT) : 5432,
        DB_NAME_TEST: envData.DB_NAME_TEST ?? "",
        DB_NAME_DEVELOPMENT: envData.DB_NAME_DEVELOPMENT ?? "",
        DB_NAME_PRODUCTION: envData.DB_NAME_PRODUCTION ?? "",
    },
    jwt: {
        JWT_SECRET: envData.JWT_SECRET ?? "9z$C&F)J@NcRfUjXn2r4u7x!A%D*G-Ka",
        JWT_REFRESH_SECRET: envData.JWT_REFRESH_SECRET ?? "jXn2r5u8x!A%D*G-KaPdSgVkYp3s6v9y",
        JWT_ISSUER: envData.JWT_ISSUER ?? "nawarajjaishi.com.np",
        JWT_TOKEN_TTL: envData.JWT_TOKEN_TTL ?? '1d',
        JWT_TOKEN_TTL_SECONDS: envData.JWT_TOKEN_TTL_SECONDS ?? 86400,
        JWT_REFRESH_TOKEN_TTL: envData.JWT_REFRESH_TOKEN_TTL ?? "7d",
    },
    mail: {
        EMAIL: envData.EMAIL ?? "example@gmail.com",
        EMAIL_PASS: envData.EMAIL_PASS ?? "",
    }
}

export default ENV_CONFIG;