import fs from 'fs';
import path from 'path';


const getDirectoryFilePath = {
    development: (dirName: string, basename: string) => {
        return getDirectoryTsFilePath(dirName, basename);
    },
    production: (dirName: string, basename: string) => {
        return getDirectoryJsFilePath(dirName, basename);
    },
} as { [key: string]: (dirName: string, basename: string) => Array<string> }

export default getDirectoryFilePath



// IT HELPS TO GET ALL .ts FILE FULL PATH...
export const getDirectoryTsFilePath = (dirName: string, basename: string): Array<string> => {
    const files = fs.readdirSync(dirName);
    const tsFiles: string[] = [];

    for (const file of files) {
        const filePath = path.join(dirName, file);
        const stat = fs.statSync(filePath);

        if (stat.isDirectory()) {
            const subFiles = getDirectoryTsFilePath(filePath, basename);
            tsFiles.push(...subFiles);

        } else if (
            file.indexOf('.') !== 0 &&
            file !== basename &&
            file.slice(-3) === '.ts' &&
            file.indexOf('.test.ts') === -1
        ) {
            tsFiles.push(filePath);
        }
    }

    return tsFiles;
};


// IT HELPS TO GET ALL .js FILE FULL PATH...
export const getDirectoryJsFilePath = (dirName: string, basename: string): Array<string> => {
    const files = fs.readdirSync(dirName);
    const jsFiles: string[] = [];

    for (const file of files) {
        const filePath = path.join(dirName, file);
        const stat = fs.statSync(filePath);

        if (stat.isDirectory()) {
            const subFiles = getDirectoryJsFilePath(filePath, basename);
            jsFiles.push(...subFiles);

        } else if (
            file.indexOf('.') !== 0 &&
            file !== basename &&
            file.slice(-3) === '.js' &&
            file.indexOf('.test.js') === -1
        ) {
            jsFiles.push(filePath);
        }
    }

    return jsFiles;
};