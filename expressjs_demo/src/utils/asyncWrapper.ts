import { NextFunction, Request, Response } from "express";
import { CustomResponseType } from "../interfaces/apiResponse";
import HandleErrors from "./error/handleErrors";



const asyncWrapper = (fn: (req: Request, res: Response<CustomResponseType>, next: NextFunction) => void) => {

    return async (req: Request, res: Response, next: NextFunction) => {
        const newHandleError = new HandleErrors(req, res, next);

        try {
            return await fn.apply(this, [req, res, next]);

        } catch (error: any) {
            newHandleError.handle(error);


            // if (error instanceof SequelizeBaseError) return (new HandleSequelizeErrors).handleInit(error, res);
            // else if (error instanceof YupValidationError) {
            //     return next(new CustomError({
            //         statusCode: StatusCodes.BAD_REQUEST,
            //         message: error.message ?? "Data Validation Failed Error",
            //         detail: error.errors
            //     }))
            // }
            // else if (error instanceof CustomError) return (next(new CustomError(error)));
            // else return next(new CustomError({
            //     statusCode: StatusCodes.BAD_REQUEST,
            //     message: error.message ?? "Something Went Wrong, please try again!",
            //     detail: {
            //         name: error.name,
            //         stack: error.stack
            //     }
            // }));
        }
    }
};

export default asyncWrapper;