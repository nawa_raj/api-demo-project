import { DbCRUDMethodMapperType } from "../supportedMethod";


// THIS FUNCTION RETURN QUERY TO CHECK IF USER HAS PERMISSION TO ACCESS THE MODEL OR NOT...
export const user_has_permission_query = (user_id: string, method: DbCRUDMethodMapperType, table_name: string, withoutFn?: boolean) => {
    if (withoutFn) {
        return `
            SELECT 
                EXISTS (
                    SELECT 1
                    FROM auth.user u
                    LEFT JOIN auth.user_auth_group uag ON u.id = uag.user_id
                    LEFT JOIN auth.auth_group_permission agp ON uag.auth_group_id = agp.auth_group_id
                    LEFT JOIN auth.auth_permission ap ON agp.auth_permission_id = ap.id
                    LEFT JOIN public.db_model dm ON ap.db_model_id = dm.id
                    WHERE u.id = '${user_id}'
                    AND ap.method = '${method}'
                    AND dm.name = '${table_name}'
                    
                    UNION
                    
                    SELECT 1
                    FROM auth.user u
                    LEFT JOIN auth.user_auth_permission uap ON u.id = uap.user_id
                    LEFT JOIN auth.auth_permission ap ON uap.auth_permission_id = ap.id
                    LEFT JOIN public.db_model dm ON ap.db_model_id = dm.id
                    WHERE u.id = '${user_id}'
                    AND ap.method = '${method}'
                    AND dm.name = '${table_name}'
                ) AS has_permission
    `
    }

    return `SELECT auth.user_has_permission('${user_id}', '${method}', '${table_name}')`
};

