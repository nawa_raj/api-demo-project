import { User } from "../db/models/auth/User";


// RETURN USER DATA EXCEPT PASSWORD
export const getUserDataWithPassword = async (user: User) => {
    const u = user as any;
    const d = {} as any;

    Object.keys(user).forEach(key => {
        if (String(u[key]).toLowerCase() !== "password") {
            d[`${key}`] = u[key];
        }
    });

    console.log("user: ", d);
    return Promise.resolve<User>(d);
};


// THIS FUNCTION BEAUTIFY THE NAEME OF ANYTHING, LIKE FOLDERNAME, IMAGE FILE NAME, etc
export const nameChanger = (string: string): string => {
    const a =
        "àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
    const b =
        "aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
    const p = new RegExp(a.split("").join("|"), "g");

    return string
        .toString()
        .toLowerCase()
        .replace(/\s+/g, "-") // Replace spaces with -
        .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, "-and-") // Replace & with 'and'
        .replace(/[^\w-.]+/g, "") // Remove all non-word characters
        .replace(/--+/g, "-") // Replace multiple - with single -
        .replace(/^-+/, "") // Trim - from start of text
        .replace(/-+$/, ""); // Trim - from end of text
};


export const generateOrderNumber = (id: string | number): string => {
    let i = String(id);

    if (i.length < 4) return "#" + i.padStart(4, "0");
    else return "#" + i;
}