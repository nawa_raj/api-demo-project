import { NextFunction, Request, Response } from "express";
import { CustomResponseType } from "../../interfaces/apiResponse";
import { ValidationError as SequelizeValidationError, BaseError as SequelizeBaseError } from "sequelize";
import CustomError from "./customError";
import { StatusCodes } from "http-status-codes";
import { ValidationError as YupValidationError } from "yup"


// BASE CALSS TO CONTROL ERRORS...
class HandleBaseError {
    declare protected statusCode: number;
    declare protected message: string;
    declare protected detail: any;
    declare protected req: Request;
    declare protected res: Response;
    declare protected next: NextFunction;
    declare protected error: any;

    constructor(req: Request, res: Response, next: NextFunction, error: any) {
        this.statusCode = StatusCodes.INTERNAL_SERVER_ERROR;
        this.message = "Internal Server Error";
        this.detail = null;
        this.res = res;
        this.req = req;
        this.next = next;
        this.error = error;
    }
}

// THIS HELP TO SEPERATE ERRORS...
export default class HandleErrors extends HandleBaseError {

    constructor(req: Request, res: Response, next: NextFunction) {
        super(req, res, next, null);
    }

    public handle(error: any) {
        super.error = error;

        // IT HANDLE ALL SEQUELIZE ERRORS...
        if (this.error instanceof SequelizeBaseError) {
            const sequelizeError = new HandleSequelizeErrors(this.req, this.res, this.next);
            sequelizeError.handleError(this.error);
        }

        // THIS HANDLE ALL ERRORS ARRIGES FROM YUP VALIDATION...
        else if (this.error instanceof YupValidationError) {
            return this.next(new CustomError({
                statusCode: StatusCodes.BAD_REQUEST,
                message: this.error.message ?? "Data Validation Failed Error",
                detail: this.error.errors
            }))
        }

        // THIS HANDLE ALL ERRORS WHICH ARE FIRED FROM CUSTOM ERROR...
        else if (this.error instanceof CustomError) return this.next(new CustomError(this.error));

        else {
            return this.next(new CustomError({
                message: this.message,
                statusCode: this.statusCode,
                detail: this.error
            }));
        }
    }
};


// THIS CALSS HELP TO RESPOND FOR sequelize Errors...
export class HandleSequelizeErrors extends HandleBaseError {

    constructor(req: Request, res: Response, next: NextFunction) {
        super(req, res, next, null);
    }

    public handleError(error: any) {
        super.error = error;

        if (this.error instanceof SequelizeValidationError) {
            const err = this.error as SequelizeValidationError;

            const errfields = err.errors.map((e) => {
                return { message: e.message, type: e.type }
            });

            this.message = "Validation Error";
            this.detail["name"] = err.name;

            if (errfields.length <= 1) this.message = errfields[0].message;
            this.detail["error"] = errfields;

            return this.next(new CustomError({
                statusCode: StatusCodes.BAD_REQUEST,
                message: this.message,
                detail: this.detail
            }))
        }

        else {
            return this.next(new CustomError({
                message: this.message,
                statusCode: this.statusCode,
                detail: this.error,
            }));
        }
    }
};




export class HandleSequelizeErrors2 {
    declare protected resData: CustomResponseType;

    constructor() {
        this.resData = {
            status: false,
            message: "Something Went Wrong, Please Try again later!",
            detail: null,
        }
    }

    public handleInit(error: any, res: Response<CustomResponseType>) {
        if (error instanceof SequelizeValidationError) {
            const err = error as SequelizeValidationError;

            const errfields = err.errors.map((e) => {
                return { message: e.message, type: e.type }
            });

            this.resData.message = "Validation Error";
            this.resData.detail["name"] = err.name;

            if (errfields.length <= 1) this.resData.message = errfields[0].message;
            this.resData.detail["error"] = errfields;

            throw new CustomError({
                statusCode: StatusCodes.BAD_REQUEST,
                message: this.resData.message,
                detail: this.resData.detail
            })
        }

        else {
            return res.status(500).json({
                ...this.resData,
                message: error.message ?? "Something Went Wrong",
                detail: error.stack
            })
        };
    }
};