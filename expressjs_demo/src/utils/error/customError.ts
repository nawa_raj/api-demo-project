
export interface CustomErrorProps {
    message: string;
    statusCode: number;
    detail: any;
}

class CustomError extends Error {
    declare public statusCode: number;
    declare public status: boolean;
    declare public detail: any;

    constructor({ message, statusCode, detail }: CustomErrorProps) {
        super(message);
        this.statusCode = statusCode;
        this.status = false;
        this.detail = detail;
    }
};

export default CustomError;



export function customError(error: CustomErrorProps) {
    return new CustomError(error)
}