import { getDbCRUDMethod } from "../supportedMethod"

// THIS FUNCTION GENERATE LISTS FOR PERMISSION TABLE WITH ALL AVAILABLE METHODS...
export const generateMethodList = (table_name: string, db_model_id: string) => {
    return [
        {
            method: getDbCRUDMethod("get"),
            title: `can view ${table_name}`,
            db_model_id: db_model_id
        },
        {
            method: getDbCRUDMethod("post"),
            title: `can add ${table_name}`,
            db_model_id: db_model_id
        },
        {
            method: getDbCRUDMethod("patch"),
            title: `can update ${table_name}`,
            db_model_id: db_model_id
        },
        {
            method: getDbCRUDMethod("delete"),
            title: `can delete ${table_name}`,
            db_model_id: db_model_id
        },
    ]
}