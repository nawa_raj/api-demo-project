import { StatusCodes } from "http-status-codes";
import { sign, verify } from "jsonwebtoken";
import { ENV_CONFIG } from "../config";
import { JWT_PAYLOAD_KEYS } from "../constants/keys/jwtPayloadKeys";
import { IUserInRequestModel, IUserJwtPayloadModel } from "../interfaces/user.interfaces";
import CustomError from "./error/customError";


const jwtConfig = ENV_CONFIG.jwt;
const signinPayloadKey = JWT_PAYLOAD_KEYS.signin;



// PROVIDE ALL METHODS REGARDING TO GENERATE AND MANAGE JWT TOKENS...
export default class JwtToken {

    public static generateUserJWTPayload = (user: IUserInRequestModel) => {
        return {
            [signinPayloadKey.id]: user.id,
            [signinPayloadKey.email]: user.email,
            [signinPayloadKey.active]: user.active,
            [signinPayloadKey.verified]: user.verified
        }
    }

    public static getUserFromJWT = (user: IUserJwtPayloadModel) => {
        return {
            id: user.sub,
            email: user.email,
            active: user.active,
            verified: user.verified
        } as IUserInRequestModel;
    }


    public static generateToken({ secret, expireTime, payload }: GenerateTokenPropsType) {
        const token = sign(
            payload,
            secret ?? jwtConfig.JWT_SECRET,
            { issuer: jwtConfig.JWT_ISSUER, expiresIn: expireTime, }
        );

        return token;
    };


    public static verifyToken({ token, secret, invalidmMsg }: VerifyTokenPropsType) {
        const sec = secret ?? jwtConfig.JWT_SECRET;
        const returnObj = { isValid: false, data: null } as { isValid: boolean, data: any };

        verify(token, sec, { issuer: jwtConfig.JWT_ISSUER }, (err, decoded) => {
            if (decoded) {
                returnObj.isValid = true;
                returnObj.data = decoded;
            }
            if (err) {
                returnObj.isValid = true;
                returnObj.data = null;
                throw new CustomError({
                    message: invalidmMsg ?? "Invalid Token",
                    statusCode: StatusCodes.UNAUTHORIZED,
                    detail: { message: `${err.message}` }
                })
            };
        });

        return returnObj;
    }


    public static getAccessToken(user: IUserInRequestModel) {
        const tokenPayload = this.generateUserJWTPayload(user)
        const token = this.generateToken({ secret: jwtConfig.JWT_SECRET, expireTime: jwtConfig.JWT_TOKEN_TTL, payload: tokenPayload });
        return token;
    };


    public static getRefreshToken(user: IUserInRequestModel) {
        const tokenPayload = this.generateUserJWTPayload(user);
        const token = this.generateToken({ secret: jwtConfig.JWT_REFRESH_SECRET, expireTime: jwtConfig.JWT_REFRESH_TOKEN_TTL, payload: tokenPayload });
        return token;
    };


    public static verifyAccessToken(token: string): AccessTokenValidationReturnModel {
        let data = { isValid: false, userData: null } as AccessTokenValidationReturnModel;
        const d = this.verifyToken({ token, secret: jwtConfig.JWT_SECRET, invalidmMsg: "Invalid Access Token" });

        data.isValid = d.isValid;
        data.userData = this.getUserFromJWT(d.data as IUserJwtPayloadModel);

        return data;
    };


    public static verifyRefreshToken(token: string): AccessTokenValidationReturnModel {
        let data = { isValid: false, userData: null } as AccessTokenValidationReturnModel;
        const d = this.verifyToken({ token, secret: jwtConfig.JWT_REFRESH_SECRET, invalidmMsg: "Invalid Refresh Token" });

        data.isValid = d.isValid;
        data.userData = this.getUserFromJWT(d.data as IUserJwtPayloadModel);

        return data;
    };
};


interface AccessTokenValidationReturnModel {
    isValid: boolean,
    userData: IUserInRequestModel | null;
}

interface GenerateTokenPropsType {
    secret?: string,
    expireTime: number | string;
    payload: any;
}

interface VerifyTokenPropsType {
    secret?: string,
    token: string;
    invalidmMsg?: string;
}