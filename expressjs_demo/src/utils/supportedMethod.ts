import { SupportedMethod } from "../interfaces/apiResponse";


// ALL METHOD WHICH WE ARE SUPPORT...
const SUPPORTED_REQUEST_METHODS = ["get", "post", "delete", "patch", 'put'] as SupportedMethod[];

export default SUPPORTED_REQUEST_METHODS;


// MAP ALL METHOD INTO SUPPORTED METHOD...
export type DbCRUDMethodMapperType = "view" | "create" | "update" | "delete";
export const DB_CRUD_METHOD_MAPPER: { [key: string]: DbCRUDMethodMapperType } = {
    "get": "view",
    "post": "create",
    "delete": "delete",
    "patch": "update",
    "put": "update"
};


export const getDbCRUDMethod = (method: string) => {
    return DB_CRUD_METHOD_MAPPER[method.toLowerCase()];
};