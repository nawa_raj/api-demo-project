import { ITableBaseModel } from "./dbbase.interface";

export interface IUserModel extends ITableBaseModel {
    id: string,
    first_name: string,
    last_name: string,
    active: boolean,
    verified: boolean,
    email: string,
};

export interface IUserInRequestModel {
    id: string,
    active: boolean,
    verified: boolean,
    email: string,
};


export interface IUserJwtPayloadModel {
    sub: string,
    active: boolean,
    verified: boolean,
    email: string,
}