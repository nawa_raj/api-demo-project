export interface CustomResponseType {
    status: boolean;
    message: string;
    detail: any;
};


export interface CustomResponseWithPagination {
    status: boolean;
    message: string;
    detail: {
        total_pages: number,
        page: number,
        per_page: number,
        data: any,
    };
};


export type SupportedMethod = "get" | "post" | "put" | "delete" | "patch";