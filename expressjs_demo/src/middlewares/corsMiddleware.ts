import cors from "cors";
import CustomError from "../utils/error/customError";
import { StatusCodes } from "http-status-codes"
import { ENV_CONFIG } from "../config";



const whitelist = ENV_CONFIG.app.ALLOWED_ORIGINS.split(",");

const corsOptions: cors.CorsOptions = {
    origin: (origin, callback) => {

        // console.log("allowed domains: ", whitelist);
        console.log("origin: ", origin);

        if (origin && whitelist.indexOf(origin) !== -1) {
            callback(null, true);

        } else {
            callback(new CustomError({
                statusCode: StatusCodes.FORBIDDEN,
                message: "Your Origin Not allowed by CORS",
                detail: { origin }
            }));
        }
    },
    methods: ["GET", "PATCH", "POST", "DELETE", "HEAD"],
    optionsSuccessStatus: 200
}

const corsMiddleware = cors(corsOptions);


export default corsMiddleware;