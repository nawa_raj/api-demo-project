import { NextFunction, Request, Response } from "express";
import { CustomResponseType } from "../interfaces/apiResponse";
import CustomError from "../utils/error/customError";
import { getReasonPhrase, ReasonPhrases, StatusCodes } from "http-status-codes"



export default class ErrorMiddleware {

    public static handle(error: any, req: Request, res: Response<CustomResponseType>, next: NextFunction) {
        console.log("Error in Middleware with message: ", error.message);
        // console.log(error)

        if (error instanceof CustomError) {
            const { detail, message, status, statusCode } = error;
            return res.status(statusCode ?? StatusCodes.INTERNAL_SERVER_ERROR).json({ status, detail, message });
        }

        return res.status(500).json({
            status: false,
            message: error.message ?? getReasonPhrase(500),
            detail: error.stack
        });
    };


    public static pathNotFoundError(req: Request, res: Response<CustomResponseType>, next: NextFunction) {
        return res.status(StatusCodes.NOT_FOUND).json({
            status: false,
            message: ReasonPhrases.NOT_FOUND,
            detail: {
                path: req.path
            }
        })
    }
}