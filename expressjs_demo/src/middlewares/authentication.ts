import { NextFunction, Request, Response } from "express";
import { ReasonPhrases, StatusCodes } from "http-status-codes";
import SUPPORTED_REQUEST_METHODS from "../utils/supportedMethod";
import { CustomResponseType, SupportedMethod } from "../interfaces/apiResponse";
import CustomError from "../utils/error/customError";
import JwtToken from "../utils/token";



export default class AuthenticationMiddleware extends JwtToken {

    // UPDATE USERDATA INTO INCOMING REQUEST IF ACCESS TOKEN PRESENT AND VALID...
    public static isUserDataPresentAndUpdateInRequest(req: Request) {
        const authHeader = req.headers.authorization;

        // UPDATE User Data In Request IF ACCESS TOKEN PRESENT AND VALID...
        if (authHeader && authHeader.startsWith("Bearer ")) {
            const token = authHeader.split(" ")[1];
            const { isValid, userData } = super.verifyAccessToken(token);

            if (isValid && userData) {
                req.user = {
                    id: userData.id,
                    email: userData.email,
                    active: userData.active,
                    verified: userData.verified
                }

                return { status: true, msg: "Access Token Validation Success" };

            } else return { status: false, msg: "User Data Not Found in Access Token" };

        } else return { status: false, msg: "Access Token Not Found" };
    };


    // THIS HELP TO AUTHENTICATE USER ACCESS TOKEN AND GRAND AUTHORIZATION...
    public static authenticateClient(req: Request, res: Response<CustomResponseType>, next: NextFunction) {

        // CHECK REQUESTED METHOD IS SUPPORTED OR NOT...
        if (!SUPPORTED_REQUEST_METHODS.includes(req.method.toLowerCase() as SupportedMethod)) {
            return next(new CustomError({
                message: "Method Not Supported",
                statusCode: StatusCodes.METHOD_NOT_ALLOWED,
                detail: { type: ReasonPhrases.METHOD_NOT_ALLOWED }
            })
            )
        };


        // VALIDATING Bearer Token...
        const validationStatus = AuthenticationMiddleware.isUserDataPresentAndUpdateInRequest(req);
        if (validationStatus && validationStatus.status) {
            next();

        } else {
            return next(new CustomError({
                message: validationStatus.msg,
                statusCode: StatusCodes.UNAUTHORIZED,
                detail: { type: ReasonPhrases.UNAUTHORIZED }
            }));
        }
    };
}; 