import express from "express";
import { ENV_CONFIG } from "./config";
import { dbConnection } from "./db/models/index";
import corsMiddleware from "./middlewares/corsMiddleware";
import ErrorMiddleware from "./middlewares/errorMiddleware";
import apiRoutes from "./routes";



// ==============================|| INIT EXPRESS AND ITS DEFAULT MIDDLEWARES ||============================== //
const app = express();
app.use(express.json());

// SERVE STATIC FILES...
app.use('/file', express.static('uploads'));

app.use(corsMiddleware);



// ==============================|| ROUTE MIDDLEWARES ||============================== //
app.use("/api", apiRoutes);
// app.use(apiRoutes);



// ==============================|| ERROR MIDDLEWARES ||============================== //
app.use(ErrorMiddleware.pathNotFoundError);
app.use(ErrorMiddleware.handle);



// ==============================|| EXPRESS SERVER START ||============================== //
const start = async () => {
    const connect = await dbConnection();
    if (connect.status) {
        app.listen(ENV_CONFIG.app.PORT, async () => {
            console.log(`\n⚡️ Local server is listening ${ENV_CONFIG.app.APP_URL}:${ENV_CONFIG.app.PORT}\n`);
        });

    } else {
        console.log(`${connect.message}\nWith Following Error: \n`, connect.error, "\n")
    }
};

start(); 